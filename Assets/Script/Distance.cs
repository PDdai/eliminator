﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Distance : MonoBehaviour
{
    //ターゲットとの距離
    [SerializeField]
    public Transform targetObj;
    //距離表示
    [SerializeField]
    public Text distanceUI;

    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var distance = Vector3.Distance(transform.position, targetObj.position);

        if(distanceUI != null)
        {
            distanceUI.text = distance.ToString("0.00");
        }
        else
        {
            Debug.Log(distance.ToString("0.00"));
        }


    }
}
