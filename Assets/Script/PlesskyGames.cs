﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlesskyGames : MonoBehaviour
{ 
    public AudioClip plesskyGames;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
            StartCoroutine("GoNextScene");
    }

    // Update is called once per frame
    void Update()
    {
       
        
    }

    IEnumerator GoNextScene()
    {
        yield return new WaitForSecondsRealtime(1f);
        audioSource.clip = plesskyGames;
        audioSource.volume = 1f;
        audioSource.PlayOneShot(plesskyGames);
        yield return new WaitForSecondsRealtime(3f);
        SceneManager.LoadScene("Title");
        yield break;
    }
}
