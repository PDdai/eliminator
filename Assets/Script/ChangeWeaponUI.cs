﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeWeaponUI : MonoBehaviour
{
    //public Image image;
    //public Sprite[] sprite;

    [SerializeField]
    private GameObject r1Image;
    [SerializeField]
    private GameObject r2Image;

    [SerializeField]
    private GameObject l1Image;
    [SerializeField]
    private GameObject l2Image;
 

    public ShootandChangeWeapon shootandChangeWeapon;
    int sRWNumber;
    int sLWNumber;

    ////初期右装備　UI参照用
    //private ShootandChangeWeapon.RightWeapon selectedRightWeaponNumber = ShootandChangeWeapon.RightWeapon.rightArmUnit;
    private List<ShootandChangeWeapon.RightWeapon> rightWeaponList = new List<ShootandChangeWeapon.RightWeapon>();
    private List<RightWeaponUIList> rWUIList = new List<RightWeaponUIList>();

    ////初期左装備　UI参照用
    //private ShootandChangeWeapon.LeftWeapon selectedLeftWeaponNumber = ShootandChangeWeapon.LeftWeapon.leftArmUnit;
    private List<ShootandChangeWeapon.LeftWeapon> leftWeaponList = new List<ShootandChangeWeapon.LeftWeapon>();
    private List<LeftWeaponUIList> lWUIList = new List<LeftWeaponUIList>();


    public enum RightWeaponUIList
    {
        RightArmUnit,//Element0
        RightShoulderUnit,//Element1
 
    }

    public enum LeftWeaponUIList
    {
        LeftArmUnit,//Element0
        LeftShoulderUnit,//Element1
    }

    // Use this for initialization
    void Start()
    {
        rightWeaponList.Add(ShootandChangeWeapon.RightWeapon.rightArmUnit);
        leftWeaponList.Add(ShootandChangeWeapon.LeftWeapon.leftArmUnit);
        //sprite = Resources.LoadAll<Sprite>("WeaponUI");

        r1Image.SetActive(false);
        r2Image.SetActive(true);
    
        l1Image.SetActive(false);
        l2Image.SetActive(true);


    }

    // Update is called once per frame
    void Update()
    {
        sRWNumber = shootandChangeWeapon.SelectedRightWeaponNumber;
        shootandChangeWeapon.selectedRightWeapon = shootandChangeWeapon.rightWeaponList[sRWNumber];

        if (sRWNumber == 0)
        {
            //使わない
            //sprite = Resources.Load<Sprite>("Machinegun");
            //image = this.GetComponent<Image>();
            //画像を切り替えたいとき
            //image.sprite = sprite[0];
            r1Image.SetActive(false);
            r2Image.SetActive(true);
        
        }
        else if (sRWNumber == 1)
        {
            //使わない
            //sprite = Resources.Load<Sprite>("Missile");
            //image = this.GetComponent<Image>();
            //画像を切り替えたいとき
            //image.sprite = sprite[1];
            r1Image.SetActive(true);
            r2Image.SetActive(false);

        }
        else if (sRWNumber == 2)
        {
            //使わない
            //sprite = Resources.Load<Sprite>("Beam");
            //image = this.GetComponent<Image>();
            //画像を切り替えたいとき
            //image.sprite = sprite[2];
            r1Image.SetActive(true);
            r2Image.SetActive(true);

        }

        sLWNumber = shootandChangeWeapon.SelectedLeftWeaponNumber;
        shootandChangeWeapon.selectedLeftWeapon = shootandChangeWeapon.leftWeaponList[sLWNumber];

        if (sLWNumber == 0)
        {
            //使わない
            //sprite = Resources.Load<Sprite>("Machinegun");
            //image = this.GetComponent<Image>();
            //画像を切り替えたいとき
            //image.sprite = sprite[0];
            l1Image.SetActive(false);
            l2Image.SetActive(true);

        }
        else if (sLWNumber == 1)
        {
            //使わない
            //sprite = Resources.Load<Sprite>("Missile");
            //image = this.GetComponent<Image>();
            //画像を切り替えたいとき
            //image.sprite = sprite[1];
            l1Image.SetActive(true);
            l2Image.SetActive(false);
        

        }
        else if (sLWNumber == 2)
        {
            //使わない
            //sprite = Resources.Load<Sprite>("Beam");
            //image = this.GetComponent<Image>();
            //画像を切り替えたいとき
            //image.sprite = sprite[2];
            l1Image.SetActive(true);
            l2Image.SetActive(true);
        
        }
    }
}