﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningSupport : MonoBehaviour
{
    public bool battleStartFlag = false;

    private void OnTriggerEnter(Collider col)
    {
     if(col.gameObject.tag == "SupportMovement")
        {
            Destroy(col.gameObject);
            battleStartFlag = true;
        }    
    }
}
