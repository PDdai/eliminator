﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 右手ビーム兵器
/// </summary>
public class Beam : MonoBehaviour
{
    [Header("弾の種類")] public GameObject beam;
    [Header("チャージ弾の種類")] public GameObject chargedBeam;
    [Header("チャージ弾の種類")] public GameObject chargeEffect;
    [Header("弾の速度")] public float bulletSpeed;
    [Header("射出口")] public Transform spawnPoint;

    public ShootandChangeWeapon shootandChangeWeapon;//スクリプト値取得用
    private int beamShotCount = 200;
    [Range(0, 4)]
    public float chargeCount = 0;//チャージショットタメ時間
    public bool chargeFlag = false;//チャージショットフラグ
    public float shotInterval;//弾射出間隔
    public ChargingGauge chargingGauge;

    public AudioClip _Beam;
    public AudioClip ChargeShot;
    AudioSource audioSource;

    private int beamDealDamage = 2000;

    public int BeamDealDamage { get => beamDealDamage; private set => beamDealDamage = value; }

    private int chargedBeamDealDamage = 8000;

    public int ChargedBeamDealDamage { get => chargedBeamDealDamage; private set => chargedBeamDealDamage = value; }

    public int BeamShotCount//UI変更用のプロパティー
    {
        get { return this.beamShotCount; }
        private set { this.beamShotCount = value; }
    }

    ////初期装備設定
    private ShootandChangeWeapon.RightWeapon selectedRightWeapon = ShootandChangeWeapon.RightWeapon.rightArmUnit;
    //private ShootandChangeWeapon.LeftWeapon selectedLeftWeapon = ShootandChangeWeapon.LeftWeapon.leftArmUnit;

    //武器リストの配列情報取得用
    private List<ShootandChangeWeapon.RightWeapon> rightWeaponList = new List<ShootandChangeWeapon.RightWeapon>();
    //private List<ShootandChangeWeapon.LeftWeapon> leftWeaponList = new List<ShootandChangeWeapon.LeftWeapon>();

    int sRWNumber;//selectedWeaponNumber quoted from ShootandChangeWeapon. To count key "2"
    int sLWNumber;//selectedWeaponNumber quoted from ShootandChangeWeapon. To count key "1"

    Rigidbody rbPlayer;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rbPlayer = GameObject.Find("Player").GetComponent<Rigidbody>();
        rightWeaponList.Add(ShootandChangeWeapon.RightWeapon.rightArmUnit);
        //leftWeaponList.Add(ShootandChangeWeapon.LeftWeapon.leftArmUnit);
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        sRWNumber = shootandChangeWeapon.SelectedRightWeaponNumber;//選択中の武器ナンバーを代入
        //Debug.Log("SelectedNumber" + sRWNumber);
        shootandChangeWeapon.selectedRightWeapon = shootandChangeWeapon.rightWeaponList[sRWNumber];//武器リストの番号と選択武器の照合


        if (sRWNumber == 0)//ビーム
        {
            //弾丸の射出
            if (Input.GetMouseButton(1))//右クリック長押しでチャージ
            {
                chargeFlag = false;
                //Debug.Log(chargeCount);
                //チャージエフェクト発生
                GameObject chargedEffect = Instantiate(chargeEffect, transform.position,
                    Quaternion.Euler(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
                chargeCount += 1 * Time.deltaTime;//チャージ時間のカウント

                if (chargeCount >= 3f || chargingGauge.chargeFlagUI == true)//3秒以上溜めたらチャージショットON
                {

                    chargeFlag = true;
                }
            }
            else if (Input.GetMouseButtonUp(1) && chargeFlag == true)//チャージショット攻撃
            {
                audioSource.PlayOneShot(ChargeShot);
                GameObject chargedBullet = Instantiate(chargedBeam, transform.position,
                    Quaternion.Euler(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
                Rigidbody bulletRb = chargedBullet.GetComponent<Rigidbody>();
                bulletRb.AddForce(transform.forward * bulletSpeed);
                chargeCount = 0;
                chargeFlag = false;
            }
            else
            {
                chargeCount -= 0.01f * Time.deltaTime;//チャージショットカウントの減衰
                if (chargeCount > 0f)
                {
                    chargeCount = 0;//チャージショットカウントが0以下の時は0に戻す。本当は０以下にならない処理を書きたい。
                }
            }
            if (Input.GetMouseButtonDown(1))//通常時のビーム攻撃
            {
                shotInterval += 1;

                if (shotInterval % 1 == 0 && beamShotCount > 0)
                {
                    beamShotCount -= 1;
                    audioSource.PlayOneShot(_Beam);
                    GameObject bullet = Instantiate(beam, transform.position,
                        Quaternion.Euler(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
                    Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
                    bulletRb.AddForce(transform.forward * bulletSpeed);
                }
            }
        }
    }
}



