﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSelection : MonoBehaviour
{
    public GameObject playerA;
    private GameObject selectedPlayer;
    public enum selectableCharacter
        {
        PlayerA,
        PlayerB,
        PlayerC
        }
    void start()
    {
        selectedPlayer = playerA;

    }

    public void OnClick()
    {
        SceneManager.sceneLoaded += GameSceneLoaded;
        SceneManager.LoadScene("Main");

    }

    private void GameSceneLoaded(Scene next, LoadSceneMode mode)
    {
        var gameManager = GameObject.FindWithTag("Player").GetComponent<PlayerSelection>();
        //gameManager.selectedPlayer = playerA;
        Debug.Log(playerA);

        SceneManager.sceneLoaded -= GameSceneLoaded;
    }

    //public static List<selectableCharacter> playableCharacters = new List<selectableCharacter>();
    //public selectableCharacter selectedCharacter = selectableCharacter.PlayerA;
    //public static int staticSelectableCharacterNumber = 0;
    //private int selectableCharacterNumber = 0;

    //public static PlayerSelection singleton;

    //public static int gameData1 = 50;
    //public int gameData2 = 30;
    //private int gameData3 = 10;



    //  static public void DontDestroyOnLoad(GameObject playerObj)
    //  {
    //Object.DontDestroyOnLoad(playerObj);
    //playableCharacters.Add(playerObj);
    //  }

    //  static public void DestroyAll()
    //  {
    //      foreach (var playerObj in playableCharacters)
    //      {
    //          GameObject.Destroy(playerObj);
    //      }
    //      playableCharacters.Clear();
    //}

 //   void Awake()
 //   {
 //       playableCharacters.Add(selectableCharacter.PlayerA);
 //       playableCharacters.Add(selectableCharacter.PlayerB);
 //       playableCharacters.Add(selectableCharacter.PlayerC);

 //       if (selectableCharacterNumber == 0)
 //       {
 //           //スクリプトが設定されていなければゲームオブジェクト
 //           //を残しつつスクリプトを設定
 //           if (singleton == null)
 //           {
 //               DontDestroyOnLoad(gameObject);
 //               singleton = this;
 //           }
 //           else
 //           {
 //               Destroy(gameObject);
 //           }

 //       }
 //   }

 //   //　スタートボタンを押したら実行
 //   public void OnClickGameStart()
	//{
 //       //　静的メンバのデータを書き換え
 //       PlayerSelection.staticSelectableCharacterNumber = 0;
	//	//　シングルトンインスタンスにデータをセット
	//	PlayerSelection.singleton.SetPlayerCharacter(selectableCharacterNumber);
	//	SceneManager.LoadScene("Main");

 //   }


 //   public int GetPlayerNumber()
 //   {
 //       return selectableCharacterNumber;
 //   }

 //   public void SetPlayerCharacter(int data)
 //   {
 //       this.selectableCharacterNumber = data;
 //   }

  //  public int GetGameData3()
  //  {
		//return gameData3;
  //  }

  //  public void SetGameData3(int data)
  //  {
		//this.gameData3 = data;
  //  }
}