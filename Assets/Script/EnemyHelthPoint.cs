﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// ダメージ計算　無属性　攻撃力*2/3
/// 実弾ダメージ計算　攻撃力/防御力
/// ビームダメージ計算
/// 防御係数　防御力/50
/// ap減少　攻撃力＊(45-防御係数)＊3/8-25
/// </summary>
public class EnemyHelthPoint : MonoBehaviour
{
	//　敵のHP
	[SerializeField]
	private int hp = 100;
    //private int ap = 3000;//armorpoint
    //private int dc;//(攻撃力ー防御力)/50
    private float disappearTime = 2f;
    //着弾エフェクト
    public GameObject recievedBeamEffect;
    //ゲームクリアUI
    [SerializeField]
    private GameObject missionClear;
    [SerializeField]
    private GameObject missionBorder;
    [SerializeField]
    private GameObject areaOverN;
    [SerializeField]
    private GameObject areaOverS;
    [SerializeField]
    private GameObject areaOverW;
    [SerializeField]
    private GameObject areaOverE;
    [SerializeField]
    private GameObject enemyMovement;
    [SerializeField]
    private GameObject destroyEffect;
    [SerializeField]
    private GameObject fadeVolume;
    [SerializeField]
    public AudioClip targetVoice;
    [SerializeField]
    public AudioClip eliminatedVoice;

    AudioSource audioSource;
    public HealthPoint healthPoint;
    int playerHPoint;

    //　プレイヤーの攻撃力
    private int missile = 700;
    private int beam = 2000;
    private int chargedBeam = 8000;
    private int twinBeam = 1000;
    private int orbiterBeam = 100;

    public bool missionClearFlag = false;
    private bool voiceCheck = false;

    public int HP
    {
        get { return this.hp; }
        private set { this.hp = value; }
    }
    // Use this for initialization
    void Start()
    {
        recievedBeamEffect.SetActive(false);
        missionClear.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        fadeVolume.GetComponent<BGMFadeOut>().enabled = false;
        healthPoint = FindObjectOfType<HealthPoint>();
    }

    // Update is called once per frame
    void Update()
    {
        playerHPoint = healthPoint.HP;
    }

    public void SetHp(int hp)
    {
        this.hp = hp;

    }

    public int GetHp()
    {
        return hp;
    }


    private void OnCollisionEnter(Collision collision)
    {

    //    if (collision.gameObject.tag == "missile")//衝突した相手のタグがmissileなら
    //{
    //        Debug.Log("Missile Hit");
    //    hp -= missile;//
    //}
}

private void OnParticleCollision(GameObject collision)
    {
        //衝突した相手のタグがbeamなら
        if (collision.gameObject.tag == "beam")
        {
            hp -= beam;//
            GameObject beamObj = GameObject.Find("beam");
            recievedBeamEffect.SetActive(true);
            Destroy(beamObj);
            //Debug.Log(hp);
            StartCoroutine("RecievedEffect");
        }
        //衝突した相手のタグがchargedBeamなら
        if (collision.gameObject.tag == "chargedBeam")
        {
            hp -= chargedBeam;//
            GameObject chargedBeamObj = GameObject.Find("chargedBeam");
            recievedBeamEffect.SetActive(true);
            Destroy(chargedBeamObj);
            //Debug.Log(hp);
            StartCoroutine("RecievedEffect");
        }
        //衝突した相手のタグがtwinBeamなら
        if (collision.gameObject.tag == "twinBeam")
        {
            hp -= twinBeam;//
            GameObject twinBeamObj = GameObject.Find("twinBeam");
            recievedBeamEffect.SetActive(true);
            Destroy(twinBeamObj);
            //Debug.Log(hp);
            StartCoroutine("RecievedEffect");
        }
        //衝突した相手のタグがorbiterなら
        if (collision.gameObject.tag == "orbiterBeam")
        {
            hp -= orbiterBeam;//
            GameObject orbiterBeamObj = GameObject.Find("OrbiterBeam");
            recievedBeamEffect.SetActive(true);
            Destroy(orbiterBeamObj);
            //Debug.Log(hp);
            StartCoroutine("RecievedEffect");
        }

        //voiceCheckはここを一回だけ通らせるために必要
        if (hp <= 0 && !voiceCheck && playerHPoint > 0)
        {
            StartCoroutine("NearlyDead");
            voiceCheck = true;
            enemyMovement.GetComponent<EnemyMovement>().enabled = false;
            missionBorder.SetActive(false);
            areaOverN.GetComponent<AreaOver>().enabled = false;
            areaOverS.GetComponent<AreaOver>().enabled = false;
            areaOverW.GetComponent<AreaOver>().enabled = false;
            areaOverE.GetComponent<AreaOver>().enabled = false;
            destroyEffect.SetActive(true);
        }

    }

    IEnumerator RecievedEffect()
    {
        yield return new WaitForSeconds(0.5f);
        recievedBeamEffect.SetActive(false);
        yield break;
    }

        IEnumerator NearlyDead()
        {
        if (playerHPoint > 0)
        {
            Debug.Log("ゲームクリア");
            yield return new WaitForSeconds(2f);
            audioSource.volume = 1f;
            audioSource.PlayOneShot(targetVoice);
            yield return new WaitForSeconds(1f);
            audioSource.PlayOneShot(eliminatedVoice);
            yield return new WaitForSeconds(6f);
            if (playerHPoint > 0)
            {
                missionClearFlag = true;
                yield return new WaitForSeconds(1f);
                missionClear.SetActive(true);
                yield return new WaitForSeconds(3.5f);
                fadeVolume.GetComponent<BGMFadeOut>().enabled = true;
                yield return new WaitForSeconds(3.5f);
                SceneManager.LoadScene("EndRoll");
            }
        }
    }
}

