﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMFadeOut : MonoBehaviour
{
    EnemyHelthPoint enemyHelthPoint;
    public bool IsFade = false;
    //public float FadeOutSeconds = 1f;
    //bool IsFadeOut = true;
    //float FadeDeltaTime = 0;
    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        enemyHelthPoint = FindObjectOfType<EnemyHelthPoint>();
        StartCoroutine("VolumeDown");
    }

    IEnumerator VolumeDown()
    {
        audioSource.volume = 0.35f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.30f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.25f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.20f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.15f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.1f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.05f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.0f;
        yield return new WaitForSeconds(0.5f);
    }
}