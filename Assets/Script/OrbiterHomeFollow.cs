﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbiterHomeFollow : MonoBehaviour
{
    public GameObject orbiterHome;
    public GameObject orbiter;
    public float subAimSpeed = 200f;

    private void Update()
    {
        //サブ照準移動
        if (orbiter != null)
        {
            var thisPos = orbiter.transform.position;
            var targetPos = orbiterHome.transform.position;
            orbiter.transform.position = Vector3.Lerp(thisPos, targetPos, subAimSpeed * Time.deltaTime);
        }
 
    }
}
  