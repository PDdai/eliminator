﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 敵の動きを試験的に作っていくCS
/// </summary>
public class SampleForMovement : MonoBehaviour
{

    //public Transform _Player;
    //public Transform _Enemy;

    //public bool specialFrontAttackFlag = false;
    //public bool specialBackAttackFlag = false;
    //public bool distanceChecker = false;

    //public float dashDistanceAtFront = 5f;
    //public float dashDistanceFromBack = 3f;
    //public float backDashDistance = 10f;
    

    //private float speed = 10f;
    //private float nullSpeed = 0f;

    //Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        //rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //敵に対して、プレイヤーが左右どちらにいるか判定する場合
        //var _diff = _Player.position - _Enemy.transform.position;

        //var _axis = Vector3.Cross(_Enemy.transform.forward, _diff);

        //var _angle = Vector3.Angle(_Enemy.transform.forward, _diff)

        //                 * (_axis.y < 0 ? -1 : 1);


        //敵から見てプレイヤーの側面が右か左を判定する場合
        //var diff = _Enemy.transform.position - _Player.position;

        //var axis_ = Vector3.Cross(_Player.transform.forward, diff);

        //var angle = Vector3.Angle(_Player.transform.forward, diff)

        //* (axis_.y < 0 ? -1 : 1);

        //Debug.Log(angle);


        //行動判定用

        //敵から見てプレイヤーの右側面が見えている場合
        //        if (angle > 5 && angle < 160)
        //        {
        //            //Debug.Log("Right");
        //            //Vector3.upは時計回り、Vector3.downは反時計回り
        //            Vector3 axis = transform.TransformDirection(Vector3.up);
        //            //LookAtを使うと真下へ向いてしまうため、距離が近づくと、オブジェクトの挙動がおかしくなる
        //            transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
        //                _Player.transform.position.z));
        //            //Axis”軸”　プレイヤーのポジションを軸としてRotateAroundで周りを回る。
        //            transform.RotateAround(_Player.position, axis, speed * Time.deltaTime);


        //        }

        //        //敵から見てプレイヤーの左側面が見えている場合
        //        else if (angle < -5 && angle > -160)
        //        {
        //            //Debug.Log("Left");
        //            //Vector3.upは時計回り、Vector3.downは反時計回り
        //            Vector3 axis = transform.TransformDirection(Vector3.down);
        //            //LookAtを使うと真下へ向いてしまうため、距離が近づくと、オブジェクトの挙動がおかしくなる
        //            transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
        //                _Player.transform.position.z));
        //            //Axis”軸”　プレイヤーのポジションを軸としてRotateAroundで周りを回る。
        //            transform.RotateAround(_Player.position, axis, speed * Time.deltaTime);

        //        }

        //        //敵から見てプレイヤーが正面にいる場合
        //        else if (angle >= -5 && angle <= 5)
        //        {
        //            float _distance = Vector3.Distance(_Enemy.transform.position, _Player.transform.position);
        //            //Debug.Log("_distance" + _distance);

        //            //ディスタンスが５fより大きい時且、スペシャルアタック未発動時急接近する
        //            if (_distance >= dashDistanceAtFront && specialFrontAttackFlag == false)
        //            {
        //                transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
        //                   _Player.transform.position.z));
        //                this.transform.Translate(Vector3.forward * Time.deltaTime * speed);

        //                if (_distance < dashDistanceAtFront + 0.5f)
        //                {
        //                    //Debug.Log("特殊前攻撃アクション追加する");
        //                    StartCoroutine("SpecialFrontAttack");
        //                }
        //            }

        //            //コルーチンでスペシャルアタックフラグをつけた後に以下スペシャルアタック発動
        //            else if (specialFrontAttackFlag == true && _distance < backDashDistance)
        //            {
        //                //Debug.Log("SPFlag コルーチンオン" + specialFrontAttackFlag);
        //                //スペシャルアタック発動後、真後ろへ急速離脱する
        //                this.transform.Translate(Vector3.back * Time.deltaTime * speed);

        //                //バックダッシュでプレイヤーから離れる距離
        //                if (_distance > backDashDistance - 0.5f)
        //                {
        //                    this.transform.Translate(Vector3.zero * Time.deltaTime);
        //                    StartCoroutine("AfterSpecialAttackFreezeTime");
        //                }
        //            }
        //        }

        //        //敵から見てプレイヤーが背面にいる場合
        //        else //if(angle > 175 && angle < -175)
        //        {
        //            float _distance = Vector3.Distance(_Enemy.transform.position, _Player.transform.position);
        //            if (_distance >= dashDistanceFromBack && specialBackAttackFlag == false)
        //            {
        //                //プレイヤーに急速接近
        //                transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
        //                   _Player.transform.position.z));
        //                this.transform.Translate(Vector3.forward * Time.deltaTime * speed);
        //                //プレイヤーの後ろから急接近して止まる距離 本当は２fにしたい
        //                if (_distance < dashDistanceFromBack + 1f)
        //                {
        //                    Debug.Log("特殊後攻撃アクション追加する");

        //                    StartCoroutine("SpecialBackAttack");
        //                    Debug.Log(_distance);
        //                }
        //            }
        //        }
        //    }

        //    //スペシャルアタックフラグをつけるコルーチン
        //    IEnumerator SpecialFrontAttack()
        //    {
        //        Debug.Log("SpecialFrontAttack" + specialFrontAttackFlag);
        //        specialFrontAttackFlag = true;
        //        yield break;
        //    }

        //    IEnumerator SpecialBackAttack()
        //    {
        //        //ここにスペシャルバックアタックアクションを入れる
        //        Debug.Log("SpecialBackAttack");
        //        yield return new WaitForSecondsRealtime(3f);
        //        Debug.Log("AfterSpecialBackAttack CouldntMove");
        //        specialBackAttackFlag = true;
        //        StartCoroutine("AfterSpecialAttackFreezeTime");
        //        yield break;
        //    }

        //IEnumerator AfterSpecialAttackFreezeTime()
        //    {
        //        yield return new WaitForSecondsRealtime(2f);
        //        specialFrontAttackFlag = false;
        //        specialBackAttackFlag = false;
        //        yield break;
    }
}
