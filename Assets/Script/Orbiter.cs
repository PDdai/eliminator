﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbiter : MonoBehaviour
{
    //自律兵器帰還場所
    [SerializeField]
    [Header("オービターホーム")] private GameObject orbiterHome;
    //自律兵器本体
    [SerializeField]
    [Header("オービター")] public GameObject orbiter;
    //自律兵器子オブジェクト
    [SerializeField]
    [Header("オービタービーム")] public GameObject orbiterBeam;
    [Header("射出口")] public Transform spawnPoint;
    //自律兵器子オブジェクト
    private GameObject _orbiter;
    //ターゲットの座標取得用
    [SerializeField]
    [Header("ターゲット")]
    private GameObject enemy;

    public HealthPoint healthPoint;
    int hPoint;

    //public OrbiterCommander orbiterCommander;
    //int oBTSC;

    //public int OBTSC
    //{
    //    get { return this.oBTSC; }
    //    private set { this.oBTSC = value; }
    //}

    private Vector3 offset = new Vector3(0, 20f, 0);

    //オービターの攻撃ポジション
    private Vector3 orbiterDestination;
    //オービターの帰還場所
    private Vector3 returnOrbiterPosition;

    //オービター攻撃時間
    private float orbiterTimer = 0;
    //オービター攻撃インターバル
    private float orbiterInterval = 0;

    //オービター攻撃フラグ
    private bool orbiterONFlag = false;

    private bool breakFlag = false;

    //ホーミング運動方程式
    Vector3 funnelVelocity;
    Vector3 orbiterPosition;


    public Vector3 acceleration;

    private int orbiterBeamDealDamage = 500;

    public int OrbiterBeamDealDamage { get => orbiterBeamDealDamage; private set => orbiterBeamDealDamage = value; }


    private void Awake()
    {

        orbiterHome.GetComponent<BoxCollider>().enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        orbiterHome = GameObject.FindGameObjectWithTag("orbiterHome");
        orbiterBeam.GetComponent<ParticleSystem>();
        breakFlag = false;

        healthPoint = FindObjectOfType<HealthPoint>();
        //orbiterCommander = FindObjectOfType<OrbiterCommander>();
        //oBTSC = orbiterCommander.OrbiterShotCount;
    }

    void Update()
    {
        //Debug.Log("orbiterTime" + orbiterTimer);
        orbiterTimer += Time.deltaTime;
        StartCoroutine("IgnightCoroutine");
        returnOrbiterPosition = orbiterHome.transform.position;
        float distance = Vector3.Distance(enemy.transform.position,
   　　　 orbiter.transform.position);
        orbiterDestination = enemy.transform.position + offset;
        //プレイヤーHP取得用
        hPoint = healthPoint.HP;
        //Debug.Log("hp " + hPoint);


        if (orbiterTimer <= 30f && distance > 15f || distance > 13f)
        {

            orbiterInterval += Time.deltaTime;
            //Debug.Log(orbiterTimer);
            //オービターのポジション
            orbiterPosition = transform.position;
            var diff = orbiterDestination - orbiterPosition;
            this.transform.rotation = Quaternion.Slerp(transform.rotation,
                    Quaternion.LookRotation(enemy.transform.position - transform.position), 0.3f);
            transform.Translate(Vector3.forward * 50f * Time.deltaTime);
            if (distance < 17f)
            {
                orbiterONFlag = true;
                //Debug.Log(orbiterInterval);

            }

        }
       else if (orbiterTimer >= 33f)
        {
            orbiterONFlag = false;
            orbiterHome.GetComponent<BoxCollider>().enabled = true;
            if (breakFlag == true || orbiterONFlag == false)
            {
                this.transform.LookAt(orbiterHome.transform.position);
                transform.Translate(Vector3.forward * 500f * Time.deltaTime);
            }
        }

        if(hPoint <= 0)
        {
            Destroy(this.gameObject);
        }
    }

   public IEnumerator IgnightCoroutine()
    {
        if(orbiterONFlag == true && orbiterInterval > 0.5f)
        {
            StartCoroutine("OrbiterON");
        }
        yield break;
    }

    public IEnumerator OrbiterON()
    {
            if (orbiterONFlag == false)
                {
              
                breakFlag = true;
                    yield break;
                }
        
                GameObject orbiterBullet = Instantiate(orbiterBeam, transform.position, Quaternion.LookRotation(enemy.transform.position - transform.position));
                //Quaternion.Euler(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
                transform.LookAt(enemy.transform.position);
                Rigidbody bulletRb = orbiterBullet.GetComponent<Rigidbody>();
                bulletRb.AddForce(transform.forward * 100f);
                yield return new WaitForSeconds(1f);
                orbiterInterval = 0;
                //orbiterONFlag = false;

        }


    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "orbiterHome")
        {
            Destroy(orbiter.gameObject);
            //oBTSC = 1;
            orbiterHome.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
