﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssultPlayerArea : MonoBehaviour
{
    public bool StateAttackFlag = false;
    FindPlayer findPlayer;
    EnemyMovement enemyMovement;

    //ステート取得用
    EnemyController enemyController;

    private void Start()
    {
        enemyController = FindObjectOfType<EnemyController>();
        findPlayer = FindObjectOfType<FindPlayer>();
        enemyMovement = FindObjectOfType<EnemyMovement>();
    }


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StateAttackFlag = true;
            //Debug.Log("StateAttackFlag" + StateAttackFlag);
            enemyController.StateProcessor.State.Value = enemyController.StateAttack;
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StateAttackFlag = true;
            enemyController.StateProcessor.State.Value = enemyController.StateAttack;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            StateAttackFlag = false;
            enemyController.StateProcessor.State.Value = enemyController.StateChase;
        }
    }



}