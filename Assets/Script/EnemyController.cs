﻿using UnityEngine;
using System.Collections;
using EnemyState;
using EnemyWeapon;
using UnityEngine.AI;
using UniRx.Triggers;
using UniRx;

/// <summary>
/// OpeningEnemyMovementと速度パラメーター共有
/// </summary>

public class EnemyController : MonoBehaviour
{
    #region 変数宣言
    //　速度
    private Vector3 velocity;
    //　ゲーム開始時着地速度
    public float landingSpeed = 200f;
    //　移動方向
    private Vector3 direction;
    //　プレイヤーの位置
    public Transform playerTransform;
    //　目的地
    private Vector3 destination;
    //　到着フラグ
    private bool arrived = false;
    //　ゲーム開始時規定ポイントに到着フラグ
    private bool openingArrived = false;
    //　初期位置移動フラグ
    public bool openingFlag = false;
    // 特定地点へのブーストダッシュフラグ
    public bool boostDashFlag = false;
    // オーバーブースト開始フラグ
    public bool startOverBoostFlag = false;

    //　イベント移動速度
    public float moveSpeed = 20f;
    //　イベントオーバーブースト速度
    public float overBoost = 100f;

    //　ゲーム開始時ポジションのオブジェクト
    [SerializeField]
    private GameObject startPosition_Enemy;

    //　ゲーム開始後移動ポジションのオブジェクト
    [SerializeField]
    private GameObject startBattlePositon_Enemy;

    [SerializeField]
    public GameObject finishBoostPoint;

    //　派生CSのステートフラグ取得用
    FindPlayer findPlayer;
    AssultPlayerArea assultPlayerArea;
    OpeningSupport openingSupport;
    EnemyMovement enemyMovement;


    #endregion



    //変更前のステート名
    private string _prevStateName;
    private string _prevWeaponName;

    //ステート using EnemyStateを一番上で宣言
    public StateProcessor StateProcessor { get; set; } = new StateProcessor();
    public EnemyStatePosition StatePosition { get; set; } = new EnemyStatePosition();
    public EnemyStateSearch StateSearch { get; set; } = new EnemyStateSearch();
    public EnemyStateChase StateChase { get; set; } = new EnemyStateChase();
    public EnemyStateAttack StateAttack { get; set; } = new EnemyStateAttack();
    public EnemyStateEvasion StateEvasion { get; set; } = new EnemyStateEvasion();

    //ウェポン using EnemyWeaponを一番上で宣言
    public WeaponProcessor WeaponProcessor { get; set; } = new WeaponProcessor();
    public EnemyWeaponArm WeaponArm { get; set; } = new EnemyWeaponArm();
    public EnemyWeaponMissile WeaponMissile { get; set; } = new EnemyWeaponMissile();
    public EnemyWeaponSpecialFrontAttack WeaponSpecialFrontAttack { get; set; } = new EnemyWeaponSpecialFrontAttack();
    public EnemyWeaponSpecialBackAttack WeaponSpecialBackAttack { get; set; } = new EnemyWeaponSpecialBackAttack();
    public EnemyWeaponExtraAttack WeaponExtraAttack { get; set; } = new EnemyWeaponExtraAttack();


    private void Start()
    {
        //ステートの初期化
        StateProcessor.State.Value = StatePosition;
        StatePosition.ExecAction = Position;
        StateSearch.ExecAction = Search;
        StateChase.ExecAction = Chase;
        StateAttack.ExecAction = Attack;
        StateEvasion.ExecAction = Evasion;

        //ステートの値が変更されたら実行処理を行うようにする
        StateProcessor.State
            .Where(_ => StateProcessor.State.Value.GetStateName() != _prevStateName)
            .Subscribe(_ =>
            {
                //Debug.Log("Now State:" + StateProcessor.State.Value.GetStateName());
                _prevStateName = StateProcessor.State.Value.GetStateName();
                StateProcessor.Execute();
            })
            .AddTo(this);


        //ウェポンの初期化　下にそれぞれクラスを作ったクラス名を右辺に入れる
        WeaponProcessor.Weapon.Value = WeaponArm;
        WeaponArm._ExecAction = Armed;
        WeaponMissile._ExecAction = EnemyMissile;
        WeaponSpecialFrontAttack._ExecAction = SpecialFrontAttack;
        WeaponSpecialBackAttack._ExecAction = SpecialBackAttack;
        WeaponExtraAttack._ExecAction = ExtraAttack;

        //ウェポンの値が変更されたら実行処理を行うようにする
        WeaponProcessor.Weapon
            .Where(_ => WeaponProcessor.Weapon.Value.GetWeaponName() != _prevWeaponName)
            .Subscribe(_ =>
            {
                //Debug.Log("Now Weapon:" + WeaponProcessor.Weapon.Value.GetWeaponName());
                _prevWeaponName = WeaponProcessor.Weapon.Value.GetWeaponName();
                WeaponProcessor._Execute();
            })
            .AddTo(this);


        #region ゲーム開始時イベント移動用

        //他オブジェクトアタッチ済みスクリプト取得用
        findPlayer = FindObjectOfType<FindPlayer>();
        assultPlayerArea = FindObjectOfType<AssultPlayerArea>();
        openingSupport = FindObjectOfType<OpeningSupport>();
        enemyMovement = FindObjectOfType<EnemyMovement>();



        #endregion

    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// ここからそれぞれのステート設定 各々の関数内にはステート変遷条件を書いても無効
    /// 派生クラス・関数で変遷条件とステートを記載すること
    /// </summary>
    ////////////////////////////////////////////////////////////////////////////////////////////
    #region State
    public void Position()
    {
        //////　初期位置を設定
        //this.transform.position = startPosition_Enemy.transform.position;
        //openingArrived = true;

        ////Debug.Log("StateがPositionに状態遷移しました。");

        //if (openingArrived == true)
        //{

        //    //    // 戦闘開始位置への自動移動用
        //    SetDestination(startBattlePositon_Enemy.transform.position);
        //    openingFlag = true;
        //    openingArrived = false;
        //}
    }

    public void Search()
    {
        //StateProcessor.State.Value = StateSearch;
        //Debug.Log("StateがSearchに状態遷移しました。");
        //if (boostDashFlag == true)
        //{
        //    //    //オーバーブースト開始地点へ到着後
        //    Debug.Log("GoToNextPoint");
        //    SetDestination(finishBoostPoint.transform.position);
        //    startOverBoostFlag = true;
        //}
    }

    public void Chase()
    {
        if (findPlayer.StateChaseFlag == true )
        {
            StateProcessor.State.Value = StateChase;
            //Debug.Log("StateがChaseに状態遷移しました。");

        }
    }

    public void Attack()
    {

        if (findPlayer.StateChaseFlag == true && assultPlayerArea.StateAttackFlag == true)
        {
            StateProcessor.State.Value = StateAttack;
            //Debug.Log("StateがAttackに状態遷移しました。");

        }
    }

    public void Evasion()
    {
        Debug.Log("StateがEvasionに状態遷移しました。");
    }
    #endregion

    /// <summary>
    /// 以下ウェポンに関する関数
    /// </summary>
    #region Weapon
    public void Armed()
    {
      
            WeaponProcessor.Weapon.Value = WeaponArm;
            //Debug.Log("WeaponがArmedに装備変更しました。");
    }

    public void EnemyMissile()
    {
        if(enemyMovement.weaponMissileFlag == true)
        {
        WeaponProcessor.Weapon.Value = WeaponMissile;
        //Debug.Log("WeaponがMissileに装備変更しました。");
        }
    }

    public void SpecialFrontAttack()
    {
        WeaponProcessor.Weapon.Value = WeaponSpecialFrontAttack;
        //Debug.Log("WeaponがSpecialFrontAttackに装備変更しました。");
    }

    public void SpecialBackAttack()
    {
        WeaponProcessor.Weapon.Value = WeaponSpecialBackAttack;
        //Debug.Log("WeaponがSpecialBackAttackに装備変更しました。");
    }

    public void ExtraAttack()
    {
        WeaponProcessor.Weapon.Value = WeaponExtraAttack;
        //Debug.Log("WeaponがExtraAttackに装備変更しました。");
    }
    #endregion


    //　目的地を設定する
    public void SetDestination(Vector3 position)
    {
        destination = position;
    }

    //　目的地を取得する
    public Vector3 GetDestination()
    {
        //OK
        return destination;
    }
}
