﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    //EnemyController取得用
    EnemyController enemyController;
    //AssultPlayerArea取得用
    AssultPlayerArea assultPlayerArea;
    //FindPlayerr取得用
    FindPlayer findPlayer;

    //プレイヤーの位置
    public Transform _Player;
    //敵自身の位置
    public Transform _Enemy;

    //距離別攻撃フラグ
    public bool distanceChecker = false;

    //別csミサイル発射フラグ取得用
    public MissileManager missileManager;
    //別csトライデント発射フラグ取得用
    public TridentManager tridentManager;

    AudioSource audioSource;

    //プレイヤー状態取得用
    public HealthPoint healthPoint;
    int hPCheck;
    //エネミーコンポーネント無効化用
    [SerializeField]
    private GameObject enemyMovement;


    /// <summary>
    /// 以下ウェポンフラグ///////////////////////////////////////////////////////
    /// </summary>

    //Weapon:Armed
    public bool weaponArmedFlag = false;
    //Weapon:EnemyMissile
    public bool weaponMissileFlag = false;
    //特殊前攻撃フラグ
    public bool specialFrontAttackFlag = false;
    //特殊後攻撃フラグ
    public bool specialBackAttackFlag = false;
    //特殊後近距離攻撃フラグ()
    public bool extraAttackFlag = false;
   
    // ///////////////////////////////////////////////////////////////////////
    

    //特殊前攻撃発動距離
    [SerializeField]
    public float dashDistanceAtFront = 10f;
    //特殊後攻撃発動距離
    [SerializeField]
    public float dashDistanceFromBack = 10f;
    //特殊前攻撃発動後移動距離
    [SerializeField]
    public float backDashDistance = 350f;

    //ExtraAttackParticleEffect
    [SerializeField]
    [Header("ExtraAttackのパーティクル")] public GameObject assultBurst;

    //敵の移動速度
    [SerializeField]
    [Header("敵の円運動移動速度")]private float speed = 10f;
    [SerializeField]
    [Header("敵の直線運動移動速度")] private float straightSpeed = 20f;
    //移動停止用変数
    private float nullSpeed = 0f;

    //[SerializeField]
    //[Header("Sound_ExtraAttack")] public AudioClip ExtraAttackSound;

  
    void Start()
    {
        enemyController = FindObjectOfType<EnemyController>();
        assultPlayerArea = FindObjectOfType<AssultPlayerArea>();
        GameObject.FindObjectOfType<HealthPoint>();
        missileManager = FindObjectOfType<MissileManager>();
        tridentManager = FindObjectOfType<TridentManager>();
        findPlayer = FindObjectOfType<FindPlayer>();
        assultBurst.SetActive(false);
        audioSource = GetComponent<AudioSource>();
  
        hPCheck = healthPoint.HP;
    }

    // Update is called once per frame
    void Update()
    {
        hPCheck = healthPoint.HP;
        //Debug.Log("プレイヤーHP Update　" + hPCheck);
        //敵から見てプレイヤーの側面が右か左を判定する場合
        var playerPos = _Player.transform.position;
        var enemyPos = _Enemy.transform.position;
        var diff = _Enemy.transform.position - _Player.position;

        var axis_ = Vector3.Cross(_Player.transform.forward, diff);

        var angle = Vector3.Angle(_Player.transform.forward, diff)

                         * (axis_.y < 0 ? -1 : 1);
        float _distance = Vector3.Distance(enemyPos, playerPos);
        //Debug.Log("_distance" + _distance);
        //Debug.Log(angle);


        //行動判定用
        if (assultPlayerArea.StateAttackFlag == true)
            {


                //敵から見てプレイヤーの右側面が見えている場合
                if (angle > 50 && angle < 130)
                {

                  
                    //EnemyController.csウェポン切替用
                    enemyController.WeaponProcessor.Weapon.Value = enemyController.WeaponMissile;
                    //Debug.Log("weaponMissileFlag" + weaponMissileFlag);
                    //Vector3.upは時計回り、Vector3.downは反時計回り
                    Vector3 axis = transform.TransformDirection(Vector3.up);
                    //LookAtを使うと真下へ向いてしまうため、距離が近づくと、オブジェクトの挙動がおかしくなる
                    transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
                        _Player.transform.position.z));
                    //Axis”軸”　プレイヤーのポジションを軸としてRotateAroundで周りを回る。
                    transform.RotateAround(_Player.position, axis, speed * Time.deltaTime);

                //ミサイル発射コントロール
                if (!weaponMissileFlag)
                {
                    weaponMissileFlag = true;
                }
                               
            }

                //敵から見てプレイヤーの左側面が見えている場合
                else if (angle < -50 && angle > -130)
                {

                    weaponMissileFlag = true;
                    //EnemyController.csウェポン切替用
                    enemyController.WeaponProcessor.Weapon.Value = enemyController.WeaponMissile;
                    //Debug.Log("weaponMissileFlag" + weaponMissileFlag);
                    //Vector3.upは時計回り、Vector3.downは反時計回り
                    Vector3 axis = transform.TransformDirection(Vector3.down);
                    //LookAtを使うと真下へ向いてしまうため、距離が近づくと、オブジェクトの挙動がおかしくなる
                    transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
                    _Player.transform.position.z));
                    //Axis”軸”　プレイヤーのポジションを軸としてRotateAroundで周りを回る。
                    transform.RotateAround(_Player.position, axis, speed * Time.deltaTime);
                //ミサイル発射コントロール
                if (!weaponMissileFlag)
                {
                    weaponMissileFlag = true;
                }
            }

                //敵から見てプレイヤーが正面にいる場合
                else if (angle >= -50 && angle <= 50)
                {
               
                    weaponMissileFlag = false;
                    //EnemyController.csウェポン切替用
                    enemyController.WeaponProcessor.Weapon.Value = enemyController.WeaponArm;
                    //float _distance = Vector3.Distance(_Enemy.transform.position, _Player.transform.position);
                    //Debug.Log("_distance" + _distance);

                    //ディスタンスが規定値より大きい時且、スペシャルアタック未発動時急接近する
                    if (_distance >= dashDistanceAtFront && specialFrontAttackFlag == false)
                    {
                    transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
                           _Player.transform.position.z));
                    this.transform.Translate(Vector3.forward * straightSpeed * Time.deltaTime );
                 

                    if (_distance < dashDistanceAtFront + 5f)
                        {
                        //Debug.Log("特殊前攻撃アクション追加する");
                        //StartCoroutine("SpecialFrontAttack");
                        StartCoroutine("ExtraAttack");
                    }
                    }

                    //コルーチンでスペシャルアタックフラグをつけた後に以下スペシャルアタック発動
                    else if (specialFrontAttackFlag == true && _distance < backDashDistance)
                    {
                        //Debug.Log("SPFlag コルーチンオン" + specialFrontAttackFlag);
                        //スペシャルアタック発動後、真後ろへ急速離脱する
                        this.transform.Translate(Vector3.back * Time.deltaTime * speed);

                        //バックダッシュでプレイヤーから離れる距離
                        if (_distance > backDashDistance - 100f)
                        {
                            this.transform.Translate(Vector3.zero * Time.deltaTime);
                            StartCoroutine("ExtraAttackFreezeTime");
                        }
                    }
                }

                //敵から見てプレイヤーが背面にいる場合
                else //if(angle > 175 && angle < -175)
                {
              
                weaponMissileFlag = false;
                    //float _distance = Vector3.Distance(_Enemy.transform.position, _Player.transform.position);
                    if (_distance >= dashDistanceFromBack && extraAttackFlag == false)
                    {
                    //プレイヤーに急速接近
                    transform.LookAt(new Vector3(_Player.transform.position.x, _Player.transform.position.y,
                           _Player.transform.position.z));
                    this.transform.Translate(Vector3.forward * straightSpeed * Time.deltaTime);
                        //プレイヤーの後ろから急接近して止まる距離
                        if (_distance < dashDistanceFromBack + 5f)
                        {
                        //Debug.Log("特殊後攻撃アクション追加する");

                        StartCoroutine("ExtraAttack");
                        }
                    }
                }
            }
            if (hPCheck <= 0)
        {
            //Debug.Log("EnemyStop");
            enemyMovement.GetComponent<EnemyMovement>().enabled = false;
        }
        }

    //スペシャルアタックフラグをつけるコルーチン
    IEnumerator SpecialFrontAttack()
    {
        //Debug.Log("SpecialFrontAttack" + specialFrontAttackFlag);
        specialFrontAttackFlag = true;
        //EnemyController.csウェポン切替用
        enemyController.WeaponProcessor.Weapon.Value = enemyController.WeaponSpecialFrontAttack;
        //Debug.Log("WeaponSpecialFrontAttack" + enemyController.WeaponProcessor.Weapon.Value);
        yield break;
    }

    IEnumerator ExtraAttack()
    {
        //EnemyController.csウェポン切替用
        enemyController.WeaponProcessor.Weapon.Value = enemyController.WeaponExtraAttack;
        //audioSource.PlayOneShot(ExtraAttackSound);
        assultBurst.SetActive(true);
        //Debug.Log("ExtraAttack");
        yield return new WaitForSecondsRealtime(5f);
        assultBurst.SetActive(false);
        //Debug.Log("ExtraAttack CouldntMove");
        extraAttackFlag = true;
        StartCoroutine("ExtraAttackFreezeTime");
        yield break;
    }

    IEnumerator ExtraAttackFreezeTime()
    {
        yield return new WaitForSecondsRealtime(2f);
        specialFrontAttackFlag = false;
        extraAttackFlag = false;
        yield break;
    }
}
