﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionArea : MonoBehaviour
{
    public AudioClip causion;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();  
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            audioSource.playOnAwake = true;
            audioSource.loop = true;
            audioSource.clip = causion;
            audioSource.volume = 0.3f;
            audioSource.Play();

        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            audioSource.playOnAwake = false;
            audioSource.loop = false;
        }
    }

}
