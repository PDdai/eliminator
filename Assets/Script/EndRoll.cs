﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndRoll : MonoBehaviour
{
    //テキストのスクロールスピード
    [SerializeField]
    private float textScrollSpeed = 30f;
    //テキストの制限位置
    [SerializeField]
    private float textLimitPosition = 730f;
    [SerializeField]
    private AudioClip endRollSound;
    //エンドロール終了タイミング
    private bool stoppedEndRoll;
    //シーン移動
    private Coroutine endRollCoroutine;

    AudioSource audioSource;


    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = 0.020f;
        audioSource.PlayOneShot(endRollSound);
        StartCoroutine("Volume");
    }

    // Update is called once per frame
    void Update()
    {
        if (stoppedEndRoll)
        {
            endRollCoroutine = StartCoroutine(GoToNextScene());
        }
        else
        {
            if(transform.position.y <= textLimitPosition)
            {
                transform.position = new Vector2(transform.position.x,
                    transform.position.y + textScrollSpeed * Time.deltaTime);
                if (Input.GetMouseButtonDown(0))
                {
                    SceneManager.LoadScene("Introduction");
                }
            }
            else
            {
                stoppedEndRoll = true;
            }
        }
    }

    IEnumerator Volume()
    {
        //フェードインスクリプトが組めなかったので擬似仕様
        audioSource.volume = 0.1f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.15f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.20f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.30f;
        yield return new WaitForSeconds(0.5f);
        audioSource.volume = 0.35f;
    }

    IEnumerator GoToNextScene()
    {
        yield return new WaitForSeconds(5f);

        if (Input.GetMouseButtonDown(0))
        {
            StopCoroutine(endRollCoroutine);
            SceneManager.LoadScene("Introduction");
        }
        yield return null;
    }
}