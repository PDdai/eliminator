﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwinBeamUI : MonoBehaviour
{
    public TwinBeam twinBeam;
    int tBShotCount;

    void Start()
    {
        tBShotCount = twinBeam.TwinBeamShotCount;
    }

    void Update()
    {
        tBShotCount = twinBeam.TwinBeamShotCount;
        Text TBSC = GetComponent<Text>();
        TBSC.text = "" + tBShotCount;
    }
}