﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObj : MonoBehaviour
{
    public float deleteTime;
   
    public GameObject Bullet;
   
    void Start()
    {
     
        Destroy(Bullet, deleteTime);
    }

    void Update()
    {
        
    }

    private void OnParticleCollision(GameObject other)
    {

        if(other.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
    }
}