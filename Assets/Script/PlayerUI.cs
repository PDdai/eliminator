﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    const int MIN = 0;
    const int MAX = 100000;

    [SerializeField]
    private Text _HPText;

    [SerializeField]
    private AudioClip emergencySound;
    AudioSource audioSource;

    private bool emergencyFlag = false;

    public HealthPoint healthPoint;
    int hPoint;

    // Start is called before the first frame update
    void Start()
    {
        GameObject.FindObjectOfType<HealthPoint>();
        audioSource = GetComponent<AudioSource>();
        hPoint = healthPoint.HP;

        _HPText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (hPoint >= 0)
        {
            hPoint = Mathf.Clamp(healthPoint.HP, MIN, MAX);
            Text HP = GetComponent<Text>();
            HP.text = "" + hPoint;

            if (hPoint < 10000)
            {
                StartCoroutine("EmergencyCall");
            }
        }
    }

    IEnumerator EmergencyCall()
    {
        if (!emergencyFlag)
        {
            emergencyFlag = true;
            //HPが10000以下場合は、HPの色を赤にする
            _HPText.color = Color.red;
            //Debug.Log("audioSorce " + audioSource.volume);
            audioSource.playOnAwake = true;
            audioSource.clip = emergencySound;
            audioSource.volume = 1f;
            audioSource.loop = true;
            audioSource.Play();
        }
        yield break;
    }
}
