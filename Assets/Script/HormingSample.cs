﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// d = vt + 1/2at2"square"
/// d=destination v = velocity t=time a = acceleration
/// 速度vの物体がt秒後にdに進むための加速度a
/// a = 2(d-vt)/t2"square"
/// </summary>
public class HormingSample : MonoBehaviour
{
    #region 試験済
    //ホーミング運動方程式
    Vector3 missilevelocity;
    Vector3 _missilePosition;

    //ターゲットの座標取得用
    private GameObject _target;

    
    //着弾時間
    float period = 5f;
    //ホーミングタイマー
   　public float hormingTimer;

    //ミサイルデストロイフラグ
    public bool destroyFlag = false;


    Rigidbody rb;
    public Vector3 acceleration;

    Missile missile;


    private void Awake()
    {
      

        rb = this.GetComponent<Rigidbody>(); ;
        _target = GameObject.FindGameObjectWithTag("Enemy");
     
    }

    // Start is called before the first frame update
    void Start()
    {
        missile = FindObjectOfType<Missile>();
    }
    void Update()
    {
        hormingTimer += Time.deltaTime;
        Debug.Log("hormingTImer" + hormingTimer);
    }

    void FixedUpdate()
    {
        if( hormingTimer >= 2f)
        {
            Debug.Log("Rotation" + transform.rotation);
            Debug.Log("hormingTImer" + hormingTimer);
            //ミサイルのポジション
            _missilePosition = transform.position;

            rb.MovePosition(_missilePosition + missilevelocity * Time.deltaTime);

           //アップデートでもいい
            var acceleration = Vector3.zero;
            /// a = 2(d-vt)/t2"square"
            var diff = _target.transform.position - _missilePosition;
            acceleration += (diff - missilevelocity * period) * 2f
                / (period * period);
            this.transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(_target.transform.position - transform.position),0.02f);
            //this.transform.LookAt(_target.transform.position);
       
            if (acceleration.magnitude > 200f)
            {
                acceleration = acceleration.normalized * 100f;
            }

            //着弾時間を減らす
            period -= Time.deltaTime;
            if (period < 0f)
            {
                return;
            }

           
            missilevelocity += acceleration * Time.deltaTime;
            _missilePosition += missilevelocity * Time.deltaTime;
            transform.position = _missilePosition;
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Enemy")
        {
            
        }
        else if(collision.gameObject.tag == "Enemy")
        {

       
        Destroy(this.gameObject);
        Debug.Log("Hit");

             }
    }
    #endregion
    //新しく書く場合はここから
}
