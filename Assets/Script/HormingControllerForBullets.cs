﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// d = vt + 1/2at2"square"
/// d=destination v = velocity t=time a = acceleration
/// 速度vの物体がt秒後にdに進むための加速度a
/// a = 2(d-vt)/t2"square"
/// </summary>
public class HormingControllerForBullets : MonoBehaviour
{
    //ホーミング運動方程式
    Vector3 missilevelocity;
    Vector3 _missilePosition;

    //ターゲットの座標取得用
    private GameObject _target;
    //着弾時間
    float period = 5f;
    //ホーミングタイマー
    public float hormingTimer;
    //ミサイルデストロイフラグ
    public bool destroyFlag = false;
    //加速
    public Vector3 acceleration;
    //ミサイルリジッドボディ
    Rigidbody rb;
    //ミサイル取得用
    //Missile missile;
    //エネミー連動用
    EnemyController enemyController;

    //プレイヤーに攻撃したい場合
    EnemyMovement enemyMovement;

    private void Awake()
    {


        rb = this.GetComponent<Rigidbody>();
        //狙いたい敵のタグを入れる
        _target = GameObject.FindGameObjectWithTag("Player");
        enemyController = FindObjectOfType<EnemyController>();

        //プレイヤーに攻撃したい場合、関連csのEnemyMovementを取得する
        enemyMovement = FindObjectOfType<EnemyMovement>();

    }

    // Start is called before the first frame update
    void Start()
    {
        //ミサイルCS値取得用
        //missile = FindObjectOfType<Missile>();
    }
    void Update()
    {
        //ホーミングタイマーの時間計算
        hormingTimer += Time.deltaTime;
        //Debug.Log("hormingTImer" + hormingTimer);
    }

    void FixedUpdate()
    {
        if (hormingTimer >= 0f)
        {
            //ミサイルのポジション
            _missilePosition = transform.position;
            //ミサイル軌道
            rb.MovePosition(_missilePosition + missilevelocity * Time.deltaTime);

            var acceleration = Vector3.zero;
            /// a = 2(d-vt)/t2"square"
            var diff = _target.transform.position - _missilePosition;
            acceleration += (diff - missilevelocity * period) * 2f
                / (period * period);
            //ミサイルの進行方向微修正　滑らかに角度を修正するためLookAtからQuaternion.Slerpへ変更
            this.transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(_target.transform.position - transform.position), 0.5f);

            if (acceleration.magnitude < 5000f)
            {
                acceleration = acceleration.normalized * 20f;
            }

            //着弾時間を減らす
            period -= Time.deltaTime;
            if (period < 0f)
            {
                return;
            }


            missilevelocity += acceleration * Time.deltaTime;
            _missilePosition += missilevelocity * Time.deltaTime;
            transform.position = _missilePosition;
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Terrain")
        {
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
            //Debug.Log("Hit");
        }
    }
}
