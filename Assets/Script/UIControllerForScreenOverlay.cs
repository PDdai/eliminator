﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControllerForScreenOverlay : MonoBehaviour
{

    [SerializeField]
    private Transform mainAimTransform;

    private RectTransform thisRectTransform;

    private Vector3 offset = new Vector3(0, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        thisRectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        thisRectTransform.position
            = RectTransformUtility.WorldToScreenPoint(Camera.main, mainAimTransform.position + offset);
    }
}
