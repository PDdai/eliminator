﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BlinkTMP : MonoBehaviour
{
    private float num = Mathf.PI;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        TextMeshProUGUI tMP = gameObject.GetComponent<TextMeshProUGUI>();
        Material material = tMP.fontMaterial;

            //マテリアルを右クリックエディットシェーダーで対応するカテゴリを確認　_OutlineWidth
            material.SetFloat("_OutlineWidth", Mathf.Abs(Mathf.Sin(num)) * 1.5f / 5);
            num += Time.deltaTime;
        //}   
    }
}
