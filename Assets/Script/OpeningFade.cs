﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class OpeningFade : MonoBehaviour
{
    public Fade fade;
    //フェードインの完了待ちフラグ
    public bool fadeInFlag = false;
    //ゲームメインシーンへの移動フラグ
    public bool onClickStartFlag = false;
    //チュートリアルシーンへの移動フラグ
    public bool onClickTutorialFlag = false;
    //シーン移動準備完了待ちフラグ
    public bool loadSceneFlag = false;

    [SerializeField]
    private GameObject cockpitImage;
    [SerializeField]
    private GameObject titleLogo;
    [SerializeField]
    private GameObject titleCover;
    [SerializeField]
    private GameObject copyright;
    [SerializeField]
    private Image startButtonFrame;
    [SerializeField]
    private TextMeshProUGUI startButtonText;
    [SerializeField]
    private Image tutorialButtonFrame;
    [SerializeField]
    private TextMeshProUGUI tutorialButtonText;
    [SerializeField]
    private GameObject startObj;
    [SerializeField]
    private GameObject tutorialObj;



    [SerializeField]
    private AudioClip buttonSound;
    [SerializeField]
    private AudioClip voice;

    AudioSource audioSource;


    private void Start()
    {
        cockpitImage.SetActive(false);
        titleCover.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        startButtonFrame = gameObject.GetComponent<Image>();
        startButtonText = this.GetComponentInChildren<TextMeshProUGUI>();
        tutorialButtonFrame = gameObject.GetComponent<Image>();
        tutorialButtonText = this.GetComponentInChildren<TextMeshProUGUI>();

    }
    public void OnClickStart()
    {
        onClickStartFlag = true;
    }

    public void OnClickTutorial()
    {
        onClickTutorialFlag = true;
    }


    void Update()
    {
        //スタートボタンがクリックされた場合
        if (onClickStartFlag == true && fadeInFlag == false)
        {
            audioSource.PlayOneShot(buttonSound);
            fade.FadeIn(1f, () => print("フェードイン完了"));
            fadeInFlag = true;
            StartCoroutine("StartFadeOut");
        }
        //チュートリアルボタンがクリックされた場合
        else if (onClickTutorialFlag == true && fadeInFlag == false)
        {
            audioSource.PlayOneShot(buttonSound);
            fade.FadeIn(1f, () => print("フェードイン完了"));
            fadeInFlag = true;
            StartCoroutine("TutorialFadeOut");
        }
        return;
    }

    IEnumerator StartFadeOut()
    {
        if(fadeInFlag == true)
        {
           
            yield return new WaitForSecondsRealtime(1f);
            cockpitImage.SetActive(true);
            
            loadSceneFlag = true;
            fade.FadeOut(1f, () => print("フェードアウト完了"));
            startButtonText.enabled = false;
            startButtonFrame.enabled = false;
            tutorialButtonText.enabled = false;
            tutorialButtonFrame.enabled = false;
            copyright.SetActive(false);
            titleLogo.SetActive(false);
            titleCover.SetActive(true);
            tutorialObj.SetActive(false);
            audioSource.PlayOneShot(voice);
            yield return new WaitForSecondsRealtime(2.9f);
            SceneManager.LoadScene("Main");
        }
    }

    IEnumerator TutorialFadeOut()
    {
        if (fadeInFlag == true)
        {
            cockpitImage.SetActive(true);
            loadSceneFlag = true;
            startButtonText.enabled = false;
            startButtonFrame.enabled = false;
            tutorialButtonText.enabled = false;
            tutorialButtonFrame.enabled = false;
            copyright.SetActive(false);
            titleLogo.SetActive(false);
            titleCover.SetActive(true);
            startObj.SetActive(false);
            yield return new WaitForSecondsRealtime(1f);
            SceneManager.LoadScene("Tutorial");
        }
    }
}