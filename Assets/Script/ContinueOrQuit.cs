﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ContinueOrQuit : MonoBehaviour
{
    public Fade fade;

    public GameObject continueButton;
    public GameObject quitButton;

    public bool continueFlag = false;
    public bool quitFlag = false;

    public AudioClip continueSound;
    public AudioClip quitSound;
    public AudioClip causion;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = true;
        audioSource.loop = true;
        audioSource.clip = causion;
        audioSource.volume = 0.2f;
        audioSource.Play();


    }

    public void OnClickContinue()
    {
        continueFlag = true;
        if (continueFlag == true)
        {
            audioSource.PlayOneShot(continueSound);
            fade.FadeIn(1f, () => print("フェードイン完了"));
            StartCoroutine("Continue");
 
        }
    }

    public void OnClickQuit()
    {
        quitFlag = true;
         if (quitFlag == true)
        {
            audioSource.PlayOneShot(quitSound);
        
            StartCoroutine("Quit");
        }
    }

    // Update is called once per frame
    void Update()
    {
    
   
    }

    IEnumerator Continue()
    {
        yield return new WaitForSecondsRealtime(1);
        fade.FadeIn(1f, () => print("フェードイン完了"));
        yield return new WaitForSecondsRealtime(2f);
        SceneManager.LoadScene("Main");
        yield break;
    }

    IEnumerator Quit()
    {
        fade.FadeIn(1f, () => print("フェードイン完了"));
        yield return new WaitForSecondsRealtime(1.5f);
        SceneManager.LoadScene("Title");
        yield break;
    }
}
