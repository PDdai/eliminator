﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class TestPlay : MonoBehaviour
{
    [SerializeField]
    private GameObject messageFrame;
    [SerializeField]
    private GameObject greeting;
    [SerializeField]
    private GameObject boost;
    [SerializeField]
    private GameObject QB;
    [SerializeField]
    private GameObject QT;
    [SerializeField]
    private GameObject weaponChange;
    [SerializeField]
    private GameObject useWeapon;
    [SerializeField]
    private GameObject wellDone;

    private float messageTimer = 0f;
    private bool tutrialFinishFlag = false;

    private bool greetingFlag = false;
    private bool boostFlag = false;
    private bool QBFlag = false;
    private bool QTFlag = false;
    private bool weaponChangeFlag = false;

    AudioSource audioSource;
    public AudioClip successSound;

    public Fade fade;
    // Start is called before the first frame update
    void Start()
    {

        messageFrame.SetActive(true);
        //前
        greeting.SetActive(true);
        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            StartCoroutine("ReturnToTitle");
        }

        #region チュートリアルセッティング
        //前
        if (Input.GetKeyDown(KeyCode.W) && !greetingFlag)
        {
            greeting.SetActive(false);
            boost.SetActive(true);
            greetingFlag = true;
            audioSource.PlayOneShot(successSound);
        }

        //ブースト
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift) && !boostFlag && boost.activeSelf == true)
        {
            boost.SetActive(false);
            //ブースト移動指示表示
            QB.SetActive(true);
            boostFlag = true;
            audioSource.PlayOneShot(successSound);
        }
        //QB
        else if (Input.GetKeyDown(KeyCode.Space) && !QBFlag && QB.activeSelf == true)
        {
            QB.SetActive(false);
            QT.SetActive(true);
            QBFlag = true;
            audioSource.PlayOneShot(successSound);
        }
        //QT
        else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftControl) && !QTFlag && QT.activeSelf == true
            || Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftControl) && !QTFlag && QT.activeSelf == true)
        {
            QT.SetActive(false);
            weaponChange.SetActive(true);
            QTFlag = true;
            audioSource.PlayOneShot(successSound);
        }
        //weaponChange
        else if (Input.GetKeyDown(KeyCode.Alpha1) && !weaponChangeFlag && weaponChange.activeSelf == true
            || Input.GetKeyDown(KeyCode.Alpha2) && !weaponChangeFlag && weaponChange.activeSelf == true)
        {
            weaponChange.SetActive(false);
            useWeapon.SetActive(true);
            weaponChangeFlag = true;
            audioSource.PlayOneShot(successSound);
        }
        //useWeapon
        else if (Input.GetMouseButtonDown(1) && weaponChange == true && useWeapon.activeSelf == true && !tutrialFinishFlag)
        {
            useWeapon.SetActive(false);
            tutrialFinishFlag = true;
            audioSource.PlayOneShot(successSound);
        }
    
        //チュートリアル工程が終了したら
        if(tutrialFinishFlag == true)
        {
            wellDone.SetActive(true);

            //メッセージ表示を非表示にする時間
            messageTimer += Time.deltaTime;
            if (messageTimer > 10f)
            {
                //メッセージUIを非表示にする
                messageFrame.SetActive(false);
                //あとはフリープレイ
            }
        }
        #endregion
    }

    IEnumerator ReturnToTitle()
    {
        fade.FadeIn(1f, () => print("FadeIN"));
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("Title");
        yield break;
    }
}
