﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// InvokeRepeating関数を使ってミサイル発射間隔を調整
/// </summary>
public class MissileManager : MonoBehaviour
{

    //エネミーミサイル
    [SerializeField]
    [Header("ミサイルプレファブ")]private GameObject missileBullet;
    //ミサイル発射待機時間
    [SerializeField]
    private float watingTime = 3f;
    [SerializeField]
    public GameObject missileSpawnPoint;
    //ミサイル速度
    private float missileVelocity = 500f;

     public EnemyHelthPoint enemyHelthPoint;
    int hPoint;

    public HealthPoint healthPoint;
    int playerHPoint;

    EnemyMovement enemyMovement;

    //初期化
    void Awake()
    {
        healthPoint = FindObjectOfType<HealthPoint>();
        playerHPoint = healthPoint.HP;
        enemyMovement = FindObjectOfType<EnemyMovement>();
        enemyHelthPoint = FindObjectOfType<EnemyHelthPoint>();
        hPoint = enemyHelthPoint.HP;
        if (!enemyMovement.weaponMissileFlag)
        {
            // InvokeRepeating("関数名",初回呼出までの遅延秒数,次回呼出までの遅延秒数)
            InvokeRepeating("MissileFire", watingTime, watingTime);
        }

    }

    void MissileFire()
    {
        if(hPoint > 0 && playerHPoint >0 )
        {
            //Debug.Log("hPoint " + hPoint);
            hPoint = enemyHelthPoint.HP;
            playerHPoint = healthPoint.HP;
            GameObject bullet = Instantiate(missileBullet, transform.parent.position,
             Quaternion.Euler(transform.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
            Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
            bulletRb.AddForce(transform.up * missileVelocity);
            enemyMovement.weaponMissileFlag = false;
        }
    }
}
