﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AreaOver : MonoBehaviour
{

    public GameObject missionFailed;
    EnemyHelthPoint enemyHelthPoint;
    public bool missionFailedFlag = false;

    //プレイヤーHPが0またはエリアオーバー時に以下のオブジェクトを無効化
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject cameraRotateOrigin;
    [SerializeField]
    private GameObject playerBody;
    [SerializeField]
    private GameObject orbiterR;
    [SerializeField]
    private GameObject orbiterL;
    [SerializeField]
    private GameObject chargeUI;
    [SerializeField]
    private GameObject enemyMovement;

    // Start is called before the first frame update
    void Start()
    {
        missionFailed.SetActive(false);
        enemyHelthPoint = FindObjectOfType<EnemyHelthPoint>();
        //mainCamera.GetComponent<GlitchFx>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if(enemyHelthPoint.missionClearFlag != true)
            {
                Debug.Log("MissionFailed");
                missionFailed.SetActive(true);
                player.GetComponent<ShootandChangeWeapon>().enabled = false;
                playerBody.SetActive(false);
                orbiterL.SetActive(false);
                orbiterR.SetActive(false);
                chargeUI.SetActive(false);
                enemyMovement.GetComponent<EnemyMovement>().enabled = false;

                //mainCamera.GetComponent<GlitchFx>().enabled = true;
            }
        }
    }
}
