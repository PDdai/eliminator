﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 他のオブジェクトから引数を呼び出す場合は、FindObjectOfType<>()を使う
/// 呼び出し元にフラグを立てる、呼び出したいスクリプトにCS名＋変数を宣言
/// FindObjectOfTypeで引数を呼び込む（同一オブジェクト内ならGetComponentでOK）
/// </summary>

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    private float recoverSpeed = 10f;
    private float freezeTime = 5f;
    private Transform CameraTransform;
    //private bool VerticalBoosterFlag = true;

    public ShootandChangeWeapon shootandChangeWeapon;

    private Rigidbody rb;
    public float speed = 20;
    public float QBPower = 400;
    public float VBPower = 20;
    public float QTPower = 100;
    public float boostSpeed = 200;
    private Vector3 direction;

    public bool DQTflag = false;//QT方向転換フラグ
    public bool AQTflag = false;//QT方向転換フラグ
    public bool Boostflag = false;

    public GameObject BackBoosters;
    public GameObject SideBoosterL;
    public GameObject SideBoosterR;
    public GameObject verticalBoostersR;
    public GameObject verticalBoostersL;

    public Vector3 _rot;//
    public GameObject aim;//

    public AudioClip QB;
    AudioSource audioSource;

    Animator animator;

    void Awake()
    {
        rb = this.GetComponent<Rigidbody>();

    }

    // Use this for initialization
    void Start()
    {
        BackBoosters.SetActive(false);
        verticalBoostersR.SetActive(false);
        verticalBoostersL.SetActive(false);
        _rot = new Vector3(0, 0.5f, 0);
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
    }
    void FixedUpdate()
    {
        var forward = this.transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime;
        var right = this.transform.TransformDirection(Vector3.right) * speed * Time.deltaTime;
        //animator.SetBool("Boost", false);
        //速度計算
        //Debug.Log(GetComponent<Rigidbody>().velocity + "Velocity");
        if (Input.GetKey(KeyCode.W))
        {
            //animator.SetBool("Boost", true);
            rb.AddForce(forward, ForceMode.VelocityChange);
            direction = forward;
            if (Boostflag == true)
            {
             
                rb.AddForce(forward * boostSpeed, ForceMode.Force);
                verticalBoostersR.SetActive(true);
                verticalBoostersL.SetActive(true);
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(-forward, ForceMode.VelocityChange);
            direction = -forward;
            if (Boostflag == true)
            {
          
                rb.AddForce(-forward * boostSpeed, ForceMode.Force);
                verticalBoostersR.SetActive(true);
                verticalBoostersL.SetActive(true);
            }
        }
        else
        {
            direction = forward;
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(right, ForceMode.VelocityChange);
            direction = right;
            if (Boostflag == true)
            {
           
                rb.AddForce(right * boostSpeed, ForceMode.Force);
                verticalBoostersR.SetActive(true);
                verticalBoostersL.SetActive(true);
            }
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-right, ForceMode.VelocityChange);
            direction = -right;
            if (Boostflag == true)
            {
         
                rb.AddForce(-right * boostSpeed, ForceMode.Force);
                verticalBoostersR.SetActive(true);
                verticalBoostersL.SetActive(true);
            }
        }
      
    }

    void Update()
    {
        //animator.SetBool("QBForward", false);
        //クイックブースト
        if (Input.GetKeyDown(KeyCode.Space))
        {
            audioSource.PlayOneShot(QB);
            rb.AddForce(direction * QBPower, ForceMode.Impulse);
            BackBoosters.SetActive(true);
            //animator.SetBool("QBForward", true);
        }

        //垂直ブースト→ブーストダッシュ
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //rb.AddForce(new Vector3(0, VBPower, 0), ForceMode.Force);
            //verticalBoostersR.SetActive(true);
            Boostflag = true;

        }
        else
        {
            Boostflag = false;
          
        }

        //QTコマンド
        if (Input.GetKey(KeyCode.D) && Input.GetKeyDown(KeyCode.LeftControl))
        {
            
            //メインエイムの場所を移動する、
            DQTflag = true;
            audioSource.PlayOneShot(QB);
            //Debug.Log(DQTflag);
            BackBoosters.SetActive(true);
            SideBoosterL.SetActive(true);
        }

        //QTコマンド
        if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.LeftControl))
        {
            AQTflag = true;
            audioSource.PlayOneShot(QB);
            //Debug.Log(AQTflag);
            BackBoosters.SetActive(true);
            SideBoosterR.SetActive(true);
        }

        if (BackBoosters.activeSelf == true)
            { 
        StartCoroutine("Booster");
        }
 

    if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            verticalBoostersR.SetActive(false);
            verticalBoostersL.SetActive(false);
        }
    }
    public IEnumerator Booster()
    {
        if (BackBoosters.activeSelf == true)
        {
            yield return new WaitForSeconds(0.15f);
            //Debug.Log("Booster");
            BackBoosters.SetActive(false);
            SideBoosterR.SetActive(false);
            SideBoosterL.SetActive(false);
        }
    }
}
