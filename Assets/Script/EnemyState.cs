﻿using UnityEngine;
using System;
using UniRx;

/// <summary>
/// キャラクターの状態（ステート）
/// </summary>
namespace EnemyState
{
    /// <summary>
    /// ステートの実行を管理するクラス
    /// </summary>
    public class StateProcessor
    {
        //ステート本体
        public ReactiveProperty<EnemyState> State { get; set; } = new ReactiveProperty<EnemyState>();

        //実行ブリッジ
        public void Execute() => State.Value.Execute();
    }

    /// <summary>
    /// ステートのクラス
    /// </summary>
    public abstract class EnemyState
    {
        //デリゲート
        public Action ExecAction { get; set; }

        //実行処理
        public virtual void Execute()
        {
            if (ExecAction != null) ExecAction();
        }

        //ステート名を取得するメソッド
        public abstract string GetStateName();
    }



    //=================================================================================
    //以下状態クラス
    //=================================================================================


    /// <summary>
    /// ゲーム開始時、定位置へ移動する状態
    /// </summary>
    public class EnemyStatePosition : EnemyState
    {
        public override string GetStateName()
        {
            return "State:Position";
        }
    }

    /// <summary>
    /// プレイヤーを探す状態
    /// </summary>
    public class EnemyStateSearch : EnemyState
    {
        public override string GetStateName()
        {
            return "State:Search";
        }
    }


    /// <summary>
    /// 走っている状態
    /// </summary>
    public class EnemyStateChase : EnemyState
    {
        public override string GetStateName()
        {
            return "State:Chase";
        }
    }

    /// <summary>
    /// 攻撃している状態
    /// </summary>
    public class EnemyStateAttack : EnemyState
    {
        public override string GetStateName()
        {
            return "State:Attack";
        }

        public override void Execute()
        {
            //Debug.Log("Attackなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (ExecAction != null) ExecAction();
        }
    }
    /// <summary>
    /// 回避している状態
    /// </summary>
    public class EnemyStateEvasion : EnemyState
    {
        public override string GetStateName()
        {
            return "State:Evasion";
        }

        public override void Execute()
        {
            //Debug.Log("Evasionなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (ExecAction != null) ExecAction();
        }
    }
}

 