﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderingEnemyCamera : MonoBehaviour
{
    //移されるカメラのタグ指定
    private const string LOCK_ON_CAMERA_TAG_NAME = "LockOnCamera";
    //映る対象がレンダリングされているかどうか
    public bool isRendered = false;
    //ミサイル射出可能タイム
    public float sequenceOfMissileTime;

    Missile missile;

    [SerializeField]
    [Header("非ロックオン時UI")] GameObject unLockOnUI;

    [SerializeField]
    [Header("ロックオン時UI")] GameObject lockOnUI;

    public float SequenceOfMissileTime
    {
        get { return this.sequenceOfMissileTime; }
        private set { this.sequenceOfMissileTime = value; }
    }

    public bool IsRendered
    {
        get { return this.isRendered; }
        private set { this.isRendered = value; }
    }
   

    // Start is called before the first frame update
    void Start()
    {
        //ロックオンUIの初期設定
        lockOnUI.SetActive(true);
        unLockOnUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("isRendered " + isRendered);
        if (isRendered == true)
        {
            sequenceOfMissileTime += Time.deltaTime;
            //Debug.Log("sequenceOfMissileTime" + sequenceOfMissileTime);

        }
        isRendered = false;
    }

    void OnWillRenderObject()
    {
        //ロックオンカメラに移ったときだけisRenderedをオン
        if (Camera.current.tag == LOCK_ON_CAMERA_TAG_NAME)
        {
            //非ロックオンUIをオフ　ロックオンUIをオン
            lockOnUI.SetActive(false);
            unLockOnUI.SetActive(true);
            //レンダリング判定をオン
            isRendered = true;
        }
        //対象がレンダリングされていない時
        else if (isRendered == false)
        {
            lockOnUI.SetActive(true);
            unLockOnUI.SetActive(false);
            sequenceOfMissileTime = 0;
        }
    }
}

