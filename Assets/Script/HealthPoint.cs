﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthPoint : MonoBehaviour
{

    //　自機のHP
    [SerializeField]
    private int hp = 80000;
    //private int ap = 3000;//armorpoint
    //private int dc;//(攻撃力ー防御力)/50
    [SerializeField]
    private GameObject mainCamera;
    [SerializeField]
    private GameObject missionFaild;

    //プレイヤーHPが0またはエリアオーバー時に以下のオブジェクトを無効化
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject cameraRotateOrigin;
    [SerializeField]
    private GameObject playerBody;
    [SerializeField]
    private GameObject orbiterR;
    [SerializeField]
    private GameObject orbiterL;
    [SerializeField]
    private GameObject chargeUI;
    [SerializeField]
    private GameObject playerUI;

    AssultBurst assultBurst;
    TridentManager tridentManager;
    public EnemyHelthPoint enemyHelthPoint;
    int enemyHPoint;



    public int HP
    {
        get { return this.hp; }
        private set { this.hp = value; }
    }


    //　敵の攻撃力
    [SerializeField]
    public int _assultBurst;
    [SerializeField]
    public int _missile;
    [SerializeField]
    public int _trident;


    public void SetHp(int hp)
    {
        this.hp = hp;
    }

    public int GetHp()
    {
        return hp;
    }

    void Start()
    {
        _assultBurst = 80000;
        _missile = 500;
        _trident = 2000;
        assultBurst = FindObjectOfType<AssultBurst>();
        tridentManager = FindObjectOfType<TridentManager>();
        //Debug.Log("hp " + hp);
        mainCamera.GetComponent<GlitchFx>().enabled = false;
        enemyHelthPoint.GetComponent<EnemyHelthPoint>();
        enemyHelthPoint = FindObjectOfType<EnemyHelthPoint>();
        missionFaild.SetActive(false);
        enemyHPoint = enemyHelthPoint.HP;

    }

    void Update()
    {

        if (hp <= 0 && enemyHPoint > 0)
        {
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<PlayerRotate>().enabled = false;
            player.GetComponent<ShootandChangeWeapon>().enabled = false;
            playerBody.SetActive(false);
            orbiterL.SetActive(false);
            orbiterR.SetActive(false);
            playerUI.SetActive(false);
            cameraRotateOrigin.GetComponent<CameraRotate>().enabled = false;
            mainCamera.GetComponent<GlitchFx>().enabled = true;
            if(enemyHelthPoint)
            missionFaild.SetActive(true);
            chargeUI.SetActive(false);
        }
    }
     

public void OnCollisionEnter(Collision collision)
    {
        //衝突した相手のタグがmissileなら
        if (collision.gameObject.tag == "missile")
        {
            //hpを-_missileずつ変える
            hp -= _missile;
        }
    }

    public void OnParticleCollision(GameObject collision)
    {
    
        if(collision.gameObject.tag == "assultBurst")
        {
            //即死攻撃
            hp -= _assultBurst;
            //Debug.Log("GameOver Player Down");
        }

        if(collision.gameObject.tag == "trident")
        {
            hp -= _trident;
        }
    }
}



