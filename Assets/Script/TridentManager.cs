﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TridentManager : MonoBehaviour
{
    //トライデント
    [SerializeField]
    [Header("トライデントプレファブ")] private GameObject tridentBullet;
    //トライデント発射待機時間
    [SerializeField]
    private float watingTime = 4f;
    [SerializeField]
    public GameObject tridentSpawnPoint;
    //ミサイル速度
    private float tridentVelocity = 2000f;

    public EnemyHelthPoint enemyHelthPoint;
    int hPoint;

    public HealthPoint healthPoint;
    int playerHPoint;

    EnemyMovement enemyMovement;

    //初期化
    void Awake()
    {
        healthPoint = FindObjectOfType<HealthPoint>();
        playerHPoint = healthPoint.HP;
        enemyMovement = FindObjectOfType<EnemyMovement>();
        enemyHelthPoint = FindObjectOfType<EnemyHelthPoint>();
        hPoint = enemyHelthPoint.HP;
        if (!enemyMovement.weaponMissileFlag)
        {
            // InvokeRepeating("関数名",初回呼出までの遅延秒数,次回呼出までの遅延秒数)
            InvokeRepeating("TridentFire", watingTime, watingTime);
        }

    }

    void TridentFire()
    {
        if (hPoint > 0 && playerHPoint > 0)
        {
            //Debug.Log("hPoint " + hPoint);
            hPoint = enemyHelthPoint.HP;
            playerHPoint = healthPoint.HP;
            GameObject bullet = Instantiate(tridentBullet, transform.parent.position,
             Quaternion.Euler(transform.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
            enemyMovement.weaponMissileFlag = false;
        }
    }
}

