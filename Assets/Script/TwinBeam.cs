﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwinBeam : MonoBehaviour
{
    [Header("弾の種類")] public GameObject twinBeam;
    //[Header("チャージ弾の種類")] public GameObject chargedBeam;
    //[Header("チャージ弾の種類")] public GameObject chargeEffect;
    [Header("弾の速度")] public float bulletSpeed;
    [Header("射出口")] public Transform spawnPoint;

    public ShootandChangeWeapon shootandChangeWeapon;//スクリプト値取得用
    private int twinBeamShotCount = 400;

    public float shotInterval;//弾射出間隔


    public AudioClip _tBeam;
    //public AudioClip ;
    AudioSource audioSource;

    private int twinBeamDealDamage = 1000;

    public int TwinBeamDealDamage { get => twinBeamDealDamage; private set => twinBeamDealDamage = value; }

    public int TwinBeamShotCount//UI変更用のプロパティー
    {
        get { return this.twinBeamShotCount; }
        private set { this.twinBeamShotCount = value; }
    }

    //武器リストの配列情報取得用
    //private List<ShootandChangeWeapon.RightWeapon> rightWeaponList = new List<ShootandChangeWeapon.RightWeapon>();
    private List<ShootandChangeWeapon.LeftWeapon> leftWeaponList = new List<ShootandChangeWeapon.LeftWeapon>();

    //int sRWNumber;//selectedWeaponNumber quoted from ShootandChangeWeapon. To count key "2"
    int sLWNumber;//selectedWeaponNumber quoted from ShootandChangeWeapon. To count key "1"


    // Start is called before the first frame update
    void Start()
    {
        //rightWeaponList.Add(ShootandChangeWeapon.RightWeapon.rightArmUnit);
        leftWeaponList.Add(ShootandChangeWeapon.LeftWeapon.leftArmUnit);
        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

        //選択中の武器ナンバーを代入
        sLWNumber = shootandChangeWeapon.SelectedLeftWeaponNumber;
        //Debug.Log("SelectedNumber" + sLWNumber);
        shootandChangeWeapon.selectedLeftWeapon = shootandChangeWeapon.leftWeaponList[sLWNumber];//武器リストの番号と選択武器の照合


        //ツインビーム
        if (sLWNumber == 0)
        {
            if (Input.GetMouseButtonDown(0))//通常時のビーム攻撃
            {
                shotInterval += 1;

                if (shotInterval % 1 == 0 && twinBeamShotCount > 0)
                {
                    twinBeamShotCount -= 1;
                    audioSource.PlayOneShot(_tBeam);
                    GameObject bullet = Instantiate(twinBeam, transform.position,
                        Quaternion.Euler(transform.parent.eulerAngles.x, transform.parent.eulerAngles.y, 0)) as GameObject;
                    Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
                    bulletRb.AddForce(transform.forward * bulletSpeed);
                }
            }
        }
    }
}



