﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbiterFollow : MonoBehaviour
{
    private GameObject orbiterHome;
    private Vector3 offset;

    void Awake()
    {
        orbiterHome = GameObject.FindGameObjectWithTag("Player").transform.Find("OrbiterHome").gameObject;
    }

    void Start()
    {
        offset = this.transform.position - orbiterHome.transform.position;
    }

    void Update()
    {
        this.transform.position = orbiterHome.transform.position + offset;
    }
}
