﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オープニングの移動のみをこのCSに移動
/// オープニングポジションに移動後はスクリプトが邪魔になるのでEnemyController.csと分けた
/// </summary>
public class OpeningEnemyMovement : MonoBehaviour
{
    //他スクリプト値取得用
    EnemyController enemyController;
    OpeningSupport openingSupport;

    // Start is called before the first frame update
    void Start()
    {
        //Boolのフラグ取得用
        enemyController = GameObject.FindObjectOfType<EnemyController>();
        openingSupport = GameObject.FindObjectOfType<OpeningSupport>();
    }

    private void FixedUpdate()
    {
        //ステートポジションの時
        if (enemyController.openingFlag == true)
        {
            StartCoroutine("OpeningMove");
        }

        //ステートサーチ且ゲーム開始時のイベント時
        if (enemyController.startOverBoostFlag == true)
        {
            StartCoroutine("OverBoostEvent");
        }

    }

    IEnumerator OpeningMove()
    {

        //　キネマティック操作のためにリジッドボディー取得
        var rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        //　目標地点まで等速で自動移動させる
        transform.LookAt(new Vector3(enemyController.GetDestination().x, enemyController.GetDestination().y, enemyController.GetDestination().z));
        this.transform.Translate(Vector3.forward * Time.deltaTime * enemyController.moveSpeed);
        //Debug.Log("EnemyTransform" + this.transform.position);
        if (openingSupport.battleStartFlag == true)
        {
            rb.isKinematic = false;
            //　目標地点到達後の慣性落下表現のために前方方向へオブジェクトを加速させる
            transform.GetComponent<Rigidbody>().AddForce(Vector3.back * enemyController.landingSpeed, ForceMode.Force);
            //Debug.Log("openingFlag" + openingFlag);
            enemyController.openingFlag = false;
            //　ブースト表現のための待ち時間
            yield return new WaitForSeconds(3f);
            enemyController.boostDashFlag = true;
            Debug.Log("boostDashFlag" + enemyController.boostDashFlag);
            enemyController.StateProcessor.State.Value = enemyController.StateSearch;
            yield break;
        }
    }

    IEnumerator OverBoostEvent()
    {
        transform.LookAt(new Vector3(enemyController.GetDestination().x, enemyController.GetDestination().y, enemyController.GetDestination().z));
        this.transform.Translate(Vector3.forward * Time.deltaTime * enemyController.overBoost);

        //うまく規定値に収まらないためNG　コライダーを使った方が良さそう。
        var _distance = Vector3.Distance(enemyController.finishBoostPoint.transform.position, transform.position);
        Debug.Log("_distance" + _distance);
        if (_distance < 3)
        {
            enemyController.startOverBoostFlag = false;
            transform.GetComponent<Rigidbody>().AddForce(Vector3.back * enemyController.overBoost, ForceMode.Force);
            yield break;
            //}
        }
    }
}
