﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeamUI : MonoBehaviour
{
    public Beam beam;
    //public GameObject CGauge1;
    //public GameObject CGauge2;
    int bShotCount;//beam


    // Start is called before the first frame update
    void Start()
    {
        bShotCount = beam.BeamShotCount;
        //CGauge1.SetActive(true);
        //CGauge2.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        bShotCount = beam.BeamShotCount;
        Text BSC = GetComponent<Text>();
        BSC.text = "" + bShotCount; //+ " / " + "20";
    }
}
