﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ステートChase判定用
/// </summary>
public class FindPlayer : MonoBehaviour
{
    public bool StateChaseFlag = false;
    AssultPlayerArea assultPlayerArea;
    EnemyMovement enemyMovement;

    //ステート取得用
    EnemyController enemyController;


    void Start()
    {
        enemyController = FindObjectOfType<EnemyController>();
        assultPlayerArea = FindObjectOfType<AssultPlayerArea>();
        enemyMovement = FindObjectOfType<EnemyMovement>();
        
    }


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StateChaseFlag = true;
            //Debug.Log("EnterStateChaseFlag" + StateChaseFlag);
            enemyController.StateProcessor.State.Value = enemyController.StateChase;

        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            enemyController.StateProcessor.State.Value = enemyController.StateChase;
        }

    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            enemyController.StateProcessor.State.Value = enemyController.StateSearch;
            Debug.Log("Now State:" + enemyController.StateProcessor.State.Value.GetStateName());
        }
    }
}