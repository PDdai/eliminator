﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// スカイボックスの空を回転させる
/// </summary>
public class SkyBoxRotation : MonoBehaviour
{
    public Material sky;

    [Range(0.001f, 0.1f)]
    public float rotateSpeed;

    float rotationRepeatValue;
    

    // Update is called once per frame
    void Update()
    {
        rotationRepeatValue = Mathf.Repeat(sky.GetFloat("_Rotation") + rotateSpeed, 360f);

        sky.SetFloat("_Rotation", rotationRepeatValue);

    }
}
