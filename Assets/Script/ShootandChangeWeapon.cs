﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootandChangeWeapon : MonoBehaviour
{
    Rigidbody rbPlayer;
    private PlayerController playerController;
    public int shotCount;
    public float shotInterval;

    Animator anim;

    public enum RightWeapon
    {
        rightArmUnit,//Element0
        rightShoulderUnit,//Element1
    }
    public enum LeftWeapon
    {
        leftArmUnit,//Element0
        leftShoulderUnit,//Element1
    }

    //初期装備設定
    public RightWeapon selectedRightWeapon = RightWeapon.rightArmUnit;
    public LeftWeapon selectedLeftWeapon = LeftWeapon.leftArmUnit;

    //プロパティー各武器へ切替カウントの共有のため
    /// <summary>
    /// 宣言　型名　変数名　＝　代入
    /// public 型名　変数名（キャピタルレター）
    /// get{ return this.変数名;} private set{this.変数名 = value;}
    /// ↓オートプロパティ
    /// </summary>
    public int SelectedRightWeaponNumber { get; private set; } = 0;
    public int SelectedLeftWeaponNumber { get; private set; } = 0;

    public List<RightWeapon> rightWeaponList = new List<ShootandChangeWeapon.RightWeapon>();
    public List<LeftWeapon> leftWeaponList = new List<ShootandChangeWeapon.LeftWeapon>();

    // Start is called before the first frame update
    void Start()
    {

        rbPlayer = GameObject.Find("Player").GetComponent<Rigidbody>();
        playerController = GetComponentInParent<PlayerController>();

        rightWeaponList.Add(RightWeapon.rightArmUnit);
        rightWeaponList.Add(RightWeapon.rightShoulderUnit);

        leftWeaponList.Add(LeftWeapon.leftArmUnit);
        leftWeaponList.Add(LeftWeapon.leftShoulderUnit);
    }

    // Update is called once per frame
    void Update()
    {
        //右武器の切り替え
        if (Input.GetKeyDown(KeyCode.Alpha2))//2キーを押すと
        {
            //Debug.Log("selectedRight" + SelectedRightWeaponNumber);
            selectedRightWeapon = rightWeaponList[SelectedRightWeaponNumber];
            SelectedRightWeaponNumber++;
            //selectedWeaponNumberのリストに該当する武器に切り替わる
            if (SelectedRightWeaponNumber == rightWeaponList.Count)
            {
                SelectedRightWeaponNumber = 0;
            }
        }
        //左武器の切り替え
        else if (Input.GetKeyDown(KeyCode.Alpha1))//１キーを押すと
        {
            //Debug.Log("selectedLeft" + SelectedLeftWeaponNumber);
            selectedLeftWeapon = leftWeaponList[SelectedLeftWeaponNumber];
            SelectedLeftWeaponNumber++;
            //selectedWeaponNumberのリストに該当する武器に切り替わる
            if (SelectedLeftWeaponNumber == leftWeaponList.Count)
                {
                    SelectedLeftWeaponNumber = 0;
                }
        }

  
    }
}