﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargingGauge : MonoBehaviour
{
    public Beam beam;
    public bool chargeFlagUI = false;//チャージUIが規定値に達しているかのフラグ
    public ShootandChangeWeapon shootandChangeWeapon;
    RectTransform rt;
    int sRWNumber;//現在セレクトされている武器の番号の取得用変数

    // Start is called before the first frame update
    void Start()
    {
        rt = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        //右チャージゲージ　左は現在無効
        sRWNumber = shootandChangeWeapon.SelectedRightWeaponNumber;//選択中の武器ナンバーを代入
        shootandChangeWeapon.selectedRightWeapon = shootandChangeWeapon.rightWeaponList[sRWNumber];

        if (sRWNumber == 0 && Input.GetMouseButton(1))
        {
            chargeFlagUI = false;
            rt.sizeDelta = new Vector2(rt.sizeDelta.x + 1.7f, 10f);//チャージゲージを0.35fずつ増加
            if (rt.sizeDelta.x >= 100f)//チャージゲージが100以上の場合は以下を実行
            {
                chargeFlagUI = true;
                //Debug.Log(chargeFlagUI);
                rt.sizeDelta = new Vector2(100f, 10f);//ゲージを100のまま維持する
            } 
        }
        else if (beam.chargeFlag == false)//チャージしていない時
        {
            rt.sizeDelta = new Vector2(rt.sizeDelta.x - 1f, 10f);//チャージゲージを1fずつ減少
            if (rt.sizeDelta.x <= 0f)//チャージゲージが0以下の時は以下を実行
            {
                rt.sizeDelta = new Vector2(0.0f, 10f);//0以下の数値にならないようにする
            }
            else if(chargeFlagUI == true)//チャージショット発射後にチャージゲージを0にする条件式
            {
                rt.sizeDelta = new Vector2(0.0f, 10f);//チャージゲージを0にする
            }
        }
    }
}

