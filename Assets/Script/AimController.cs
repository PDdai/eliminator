﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    
    public GameObject aimRotateOrigin;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var aimRot = aimRotateOrigin.transform.rotation;
        this.transform.rotation = aimRot;

    }
}

