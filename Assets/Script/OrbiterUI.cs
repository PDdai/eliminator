﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class OrbiterUI : MonoBehaviour
{
    public OrbiterCommander orbiterCommander;
    int oBTShotCount;

    void Start()
    {
        oBTShotCount = orbiterCommander.OrbiterShotCount;
    }

    void Update()
    {
        oBTShotCount = orbiterCommander.OrbiterShotCount;
        Text OBTSC = GetComponent<Text>();
        OBTSC.text = "" + oBTShotCount;
    }
}
