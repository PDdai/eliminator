﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbiterCommander : MonoBehaviour
{
    [SerializeField]
    private GameObject _orbiter;

    private int orbiterShotCount = 1;
    private float orbiterReloadTimer = 0;
    private bool orbiterReloadFlag = false;
    //オービタルUIへ値渡し用
    public int OrbiterShotCount
    {
        get { return this.orbiterShotCount; }
        private set { this.orbiterShotCount = value; }
    }

    //public Orbiter orbiter;
    //int oBTSCReload;

    Rigidbody rb;

    public ShootandChangeWeapon shootandChangeWeapon;//スクリプト値取得用

    //public AudioClip _Beam;
    //public AudioClip ChargeShot;
    //AudioSource audioSource;

    //private int beamDealDamage = 2000;

    //public int BeamDealDamage { get => beamDealDamage; private set => beamDealDamage = value; }

    //private int chargedBeamDealDamage = 6000;

    //public int ChargedBeamDealDamage { get => chargedBeamDealDamage; private set => chargedBeamDealDamage = value; }

    //public int BeamShotCount//UI変更用のプロパティー
    //{
    //    get { return this.beamShotCount; }
    //    private set { this.beamShotCount = value; }
    //}

    //武器リストの配列情報取得用
    //private List<ShootandChangeWeapon.RightWeapon> rightWeaponList = new List<ShootandChangeWeapon.RightWeapon>();
    private List<ShootandChangeWeapon.LeftWeapon> leftWeaponList = new List<ShootandChangeWeapon.LeftWeapon>();

    //int sRWNumber;//selectedWeaponNumber quoted from ShootandChangeWeapon. To count key "2"
    int sLWNumber;//selectedWeaponNumber quoted from ShootandChangeWeapon. To count key "1"


    // Start is called before the first frame update
    void Start()
    {
        //rightWeaponList.Add(ShootandChangeWeapon.RightWeapon.rightArmUnit);
        leftWeaponList.Add(ShootandChangeWeapon.LeftWeapon.leftArmUnit);
        //orbiter = FindObjectOfType<Orbiter>();
        //audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("orbiterShotCount " + orbiterShotCount);
        //Debug.Log("orbiterReloadTime " + orbiterReloadTimer);
        //Debug.Log("obTSCReload " + oBTSCReload);
        //oBTSCReload = orbiter.OBTSC;
        //orbiterShotCount = oBTSCReload;
        //選択中の武器ナンバーを代入
        sLWNumber = shootandChangeWeapon.SelectedLeftWeaponNumber;
        //Debug.Log("SelectedNumber" + sLWNumber);
        shootandChangeWeapon.selectedLeftWeapon = shootandChangeWeapon.leftWeaponList[sLWNumber];//武器リストの番号と選択武器の照合

        if (orbiterReloadFlag)
        {
            orbiterReloadTimer += Time.deltaTime;
            if(orbiterReloadTimer > 40)
            {
                orbiterShotCount = 1;
                orbiterReloadTimer = 0;
                orbiterReloadFlag = false;
            }
        }
   
   

        if (sLWNumber == 1)
        {
            if (Input.GetMouseButtonDown(0) && orbiterShotCount > 0)
            {
                orbiterShotCount -= 1;
                GameObject orbiter = GameObject.Instantiate(_orbiter,
                           transform.position, Quaternion.Euler(transform.parent.eulerAngles.x,
                           transform.parent.eulerAngles.y, 0)) as GameObject;
                orbiterReloadFlag = true;

               
            }
        }
      
    }
}
