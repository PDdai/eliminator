﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MissionClearFade : MonoBehaviour
{

    //テキストのフェードイン
    [SerializeField]
    private float missionClearInTime = 3.0f;
    [Header("フェードインスピード")]public float fadeSpeed = 0.02f;
    [Header("テキストカラー")] private Color textColor;

    //ミッションクリアオブジェクト
    public GameObject missionClear;

    public Fade fade;
    EnemyHelthPoint enemyHelthPoint;

    // Use this for initialization
    void Start()
    {
        //称号のカラーを取得してアルファを０に初期化
        textColor = this.missionClear.GetComponent<TextMeshProUGUI>().color;
        textColor.a = 0;
        this.missionClear.GetComponent<TextMeshProUGUI>().color = textColor;
        enemyHelthPoint = FindObjectOfType<EnemyHelthPoint>();

    }

    // Update is called once per frame
    void Update()
    {

        //missionClearInTime秒経ったらFadeInを呼ぶ
        Invoke("FadeIn", missionClearInTime);

        if(enemyHelthPoint.missionClearFlag)
        {
            enemyHelthPoint.missionClearFlag = false;
            StartCoroutine("ClearFade");
        }
    }

    void FadeIn()
    {
        if (textColor.a <= 1)
        {
            //アルファ値を徐々に＋する
            textColor.a += fadeSpeed;
            //テキストへ反映させる
            this.missionClear.GetComponent<TextMeshProUGUI>().color = textColor;　
        }

    }

    IEnumerator ClearFade()
    {
        //Debug.Log("clearfade");
        yield return new WaitForSeconds(6f);
        fade.FadeIn(1f, () => print("FadeIN"));
    }
}