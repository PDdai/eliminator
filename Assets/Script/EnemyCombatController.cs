﻿using UnityEngine;
using System;
using UniRx;

/// <summary>
/// ウェポンの切り替え実行を管理するクラス
/// </summary>
namespace EnemyWeapon
{
    /// <summary>
    /// ウェポンの実行を管理するクラス
    /// </summary>
    public class WeaponProcessor
    {
        //ステート本体
        public ReactiveProperty<EnemyWeapon> Weapon { get; set; } = new ReactiveProperty<EnemyWeapon>();

        //実行ブリッジ
        public void _Execute() => Weapon.Value._Execute();
    }

    /// <summary>
    /// ウェポンのクラス
    /// </summary>
    public abstract class EnemyWeapon
    {
        //デリゲート
        public Action _ExecAction { get; set; }

        //実行処理
        public virtual void _Execute()
        {
            if (_ExecAction != null) _ExecAction();
        }

        //ステート名を取得するメソッド
        public abstract string GetWeaponName();
    }

    //=================================================================================
    //以下ウェポンクラス
    //=================================================================================


    /// <summary>
    /// ゲーム開始時、武装選択
    /// </summary>
    public class EnemyWeaponArm : EnemyWeapon
    {
        public override string GetWeaponName()
        {
            return "Weapon:Armed";
        }
        public override void _Execute()
        {
            //Debug.Log("Armedなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (_ExecAction != null) _ExecAction();
        }
    }

    /// <summary>
    /// 武装選択 ミサイル
    /// </summary>
    public class EnemyWeaponMissile : EnemyWeapon
    {
        public override string GetWeaponName()
        {
            return "Weapon:EnemyMissile";
        }
        public override void _Execute()
        {
            //Debug.Log("EnemyMissileなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (_ExecAction != null) _ExecAction();
        }
    }

    /// <summary>
    /// 武装選択 前特殊攻撃
    /// </summary>
    public class EnemyWeaponSpecialFrontAttack : EnemyWeapon
    {
        public override string GetWeaponName()
        {
            return "Weapon:SpecialFrontAttack";
        }
        public override void _Execute()
        {
            //Debug.Log("FrontAttackなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (_ExecAction != null) _ExecAction();
        }
    }

    /// <summary>
    /// 武装選択 後特殊攻撃
    /// </summary>
    public class EnemyWeaponSpecialBackAttack : EnemyWeapon
    {
        public override string GetWeaponName()
        {
            return "Weapon:SpecialBackAttack";
        }
        public override void _Execute()
        {
            //Debug.Log("BackAttackなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (_ExecAction != null) _ExecAction();
        }
    }

    /// <summary>
    /// 武装選択 後近距離特殊攻撃
    /// </summary>
    public class EnemyWeaponExtraAttack : EnemyWeapon
    {
        public override string GetWeaponName()
        {
            return "Weapon:ExtraAttack";
        }
        public override void _Execute()
        {
            //Debug.Log("ExtraAttackなにか特別な処理をしたいときは派生クラスにて処理をしても良い");
            if (_ExecAction != null) _ExecAction();
        }
    }
}

