﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimTarget : MonoBehaviour
{
    public GameObject AimDirection;

    protected virtual void FixedUpdate()
    {
        if(AimDirection != null)
        {
            Vector3 target_pos = new Vector3(AimDirection.transform.position.x, AimDirection.transform.position.y, AimDirection.transform.position.z);
            Vector3 arm_pos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            Vector3 direction = target_pos = arm_pos;
            float angle = -(Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg) - 90f;
            Debug.Log(direction);
        }
    }
}